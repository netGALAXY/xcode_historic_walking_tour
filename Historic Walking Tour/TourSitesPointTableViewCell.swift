//
//  TourSitesPointTableViewCell.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 7/25/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit

class TourSitesPointTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func populate(forPoint:Point) {
        var numberString = NSMutableAttributedString(string: "\(forPoint.tourSiteOrder). ")
        let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
        var addressString = ""
        if let address = forPoint.address {
            addressString = address
        }
        let underlineAttributedString = NSAttributedString(string: forPoint.title + " | " + addressString, attributes: underlineAttribute)
        numberString.append(underlineAttributedString)
        titleLabel.attributedText = numberString
    }
}
