//
//  PointTableViewCell.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 7/7/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit

class PointTableViewCell: UITableViewCell {

    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pointImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func populate (forPoint:Point?, forCity:City?) {
        if let point = forPoint {
            point.getImage(forAd: false, forRulerAd: false) { (image) in
                if let im = image {
                    self.pointImageView.image = im
                }
            }
            if point.isAdvertiser {
                contentView.backgroundColor = Config.current().adColor
                titleLabel.textColor = UIColor.white
                addressLabel.textColor = UIColor.white
            }else {
                contentView.backgroundColor = UIColor.white
                titleLabel.textColor = UIColor.black
                addressLabel.textColor = UIColor.black
            }
            
            titleLabel.text = point.title
            addressLabel.text = point.address
            distanceLabel.text = ""
            point.getDistanceString { (distanceString) in
                self.distanceLabel.text = distanceString
            }
            if point.currentUserHasAccess() {
                for view in subviews {
                    view.alpha = 1
                }
            }else {
                for view in subviews {
                    view.alpha = 0.5
                }
            }
        }else if let city = forCity {
            for view in subviews {
                view.alpha = city.isPurchased() ? 1 : 0.5
            }
            titleLabel.text = "History of " + city.title
            addressLabel.text = ""
            distanceLabel.text = ""
            city.getImage({ (image) in
                if let image = image {
                    self.pointImageView.image = image
                }
            })
        }else {
            for view in subviews {
                view.alpha = 1
            }
            pointImageView.contentMode = .scaleAspectFit
            pointImageView.image = #imageLiteral(resourceName: "premium_upgrade_btn")
            titleLabel.text = "Purchase premium content"
            addressLabel.text = ""
            distanceLabel.text = ""
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        pointImageView.image = #imageLiteral(resourceName: "placeholder")
        contentView.backgroundColor = UIColor.white
    }
}
