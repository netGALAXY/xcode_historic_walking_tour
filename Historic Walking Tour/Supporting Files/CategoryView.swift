//
//  CategoryView.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 7/24/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit

protocol CategoryLabelDelegate {
    func tapped(categoryLabel:CategoryLabel)
}
class CategoryLabel: UILabel {
    var category:Category!
    var delegate:CategoryLabelDelegate!
    var position = 0
    
    class func addCategories (categories:[Category], forScrollView:UIScrollView, delegate:CategoryLabelDelegate) -> CategoryLabel {
        var x = CGFloat(0)
        let y = CGFloat(0)
        let height = CGFloat(forScrollView.frame.size.height)
        var allCategoryLabel:CategoryLabel!
        
        var currentPosition = 0
        for category in categories {
            let label = CategoryLabel(frame: CGRect(x: x, y: y, width: 100, height: height))
            label.text = "  " + category.title + "  "
            label.font = UIFont(name: "AvenirNext-Regular", size: 14.0)
            label.sizeToFit()
            label.frame.size.height = height
            x += label.frame.size.width
            label.delegate = delegate
            label.category = category
            label.addGestureRecognizer(UITapGestureRecognizer(target: label, action: #selector(tapped(tap:))))
            label.isUserInteractionEnabled = true
            if category.isAll() {
                label.backgroundColor = #colorLiteral(red: 0.1450145841, green: 0.2887423635, blue: 0.6055315733, alpha: 1)
                allCategoryLabel = label
            }else {
                label.backgroundColor = #colorLiteral(red: 0.2005214393, green: 0.6904510856, blue: 0.9354477525, alpha: 1)
            }
            label.textColor = UIColor.white
            label.layer.borderColor = #colorLiteral(red: 0.1450145841, green: 0.2887423635, blue: 0.6055315733, alpha: 1).cgColor
            label.layer.borderWidth = 0.5
            forScrollView.addSubview(label)
            forScrollView.contentSize = CGSize(width: x, height: height)
            label.position = currentPosition
            currentPosition += 1
        }
        forScrollView.isScrollEnabled = true
        return allCategoryLabel
    }
    
    @objc func tapped(tap:UITapGestureRecognizer) {
        delegate.tapped(categoryLabel: self)
    }

}
