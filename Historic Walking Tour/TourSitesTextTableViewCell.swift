//
//  TourSitesTextTableViewCell.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 7/25/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit
import FRHyperLabel

class TourSitesTextTableViewCell: UITableViewCell {
    var loaded = false
    @IBOutlet weak var htmlTextLabel: FRHyperLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func populate(forCity:City, viewController:UIViewController, tableView:UITableView, forIndexPath:IndexPath) {
        MethodHelper.showHudWithMessage(message: "Loading...", view: viewController.view)
        htmlTextLabel.loadHtml(htmlString: forCity.tourSitesText) { (success) in
            if !self.loaded {
                MethodHelper.showHudWithMessage(message: "Loading...", view: viewController.view)
                self.loaded = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    tableView.beginUpdates()
                    tableView.reloadRows(at: [forIndexPath], with: .none)
                    tableView.endUpdates()
                    MethodHelper.hideHUD()
                    self.htmlTextLabel.sizeToFit()
                })
            }else {
                MethodHelper.hideHUD()
            }
//            self.htmlTextLabel.sizeToFit()
//            self.htmlTextLabel.setLinkForSubstring("Click here to read a brief history of " + forCity.title + ".", withLinkHandler: { (label, string) in
//                print("CLICKED_LINK")
//                CityHistoryViewController.display(viewController: viewController, navigationController: viewController.navigationController, city: forCity)
//            })
            self.htmlTextLabel.isUserInteractionEnabled = false
            
        }
    }
}
