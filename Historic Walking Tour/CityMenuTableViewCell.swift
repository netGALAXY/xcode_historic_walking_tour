//
//  CityMenuTableViewCell.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 7/5/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit

class CityMenuTableViewCell: UITableViewCell {
    @IBOutlet weak var menuImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
