//
//  PushNotificationSupport.swift
//  Fort Sumter Tours
//
//  Created by Charles von Lehe on 2/28/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import Foundation
import UserNotifications
import Parse

extension AppDelegate {
    func setupPushNotifications (application: UIApplication, launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
        let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        application.registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.alert, .sound]) { (granted, errorO) in
                
            }
            center.delegate = self
        }
        performActionForNotification(notificationO: launchOptions?[UIApplicationLaunchOptionsKey.localNotification] as? UILocalNotification)
        print("didFinishLaunchingWithOptions")
        if let options = launchOptions {
            if let userInfo = options[UIApplicationLaunchOptionsKey.remoteNotification] as? [NSObject:AnyObject] {
                self.application(application: application, didReceiveRemoteNotification: userInfo)
            }
        }
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        performActionForNotification(notificationO: notification)
    }
    
    func performActionForNotification (notificationO:UILocalNotification?) {
        print("performActionForNotification:", notificationO ?? "")
    }
    
    private func registerForPushNotifications (application: UIApplication, launchOptions:[UIApplicationLaunchOptionsKey : Any]?) {
        // Register for Push Notitications
        // Register for Push Notitications
        if application.applicationState != UIApplicationState.background {
            // Track an app open here if we launch with a push, unless
            // "content_available" was used to trigger a background push (introduced in iOS 7).
            // In that case, we skip tracking here to avoid double counting the app-open.
            
            let preBackgroundPush = !application.responds(to: #selector(getter: UIApplication.backgroundRefreshStatus))
            let oldPushHandlerOnly = !self.responds(to: #selector(UIApplicationDelegate.application(_:didReceiveRemoteNotification:fetchCompletionHandler:)))
            var pushPayload = false
            if let options = launchOptions {
                pushPayload = options[UIApplicationLaunchOptionsKey.remoteNotification] != nil
            }
            if (preBackgroundPush || oldPushHandlerOnly || pushPayload) {
                PFAnalytics.trackAppOpened(launchOptions: launchOptions)
            }
        }
        if application.responds(to: #selector(UIApplication.registerUserNotificationSettings(_:))) {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        } else {
            let types = UIRemoteNotificationType.alert.union(UIRemoteNotificationType.badge.union(UIRemoteNotificationType.sound))
            application.registerForRemoteNotifications(matching: types)
        }
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        print("DID_RECEIVE_PUSH1 - USER_INFO:",userInfo)
        processNotification(application, userInfo: userInfo)
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        PFPush.handle(userInfo)
        print("DID_RECEIVE_PUSH2 - USER_INFO:",userInfo)
        if application.applicationState == .active, let dict = (userInfo as NSDictionary) as? [String:AnyObject] {
            if let message = dict["aps"] as? String {
                MethodHelper.showUNNotification(title: "Historic Walking Tour", message: message)
            }
        }
        processNotification(application, userInfo: userInfo)

    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        processNotification(application, userInfo: userInfo)
    }
    
    func processNotification (_ application: UIApplication, userInfo: [AnyHashable : Any]) {
        if ( application.applicationState == .inactive || application.applicationState == .background  )
        {
            print("RECEIVED1")
            handleTappedNotification(userInfo: userInfo)
            
            //opened from a push notification when the app was on background
            
        }
    }
    
    func handleTappedNotification (userInfo: [AnyHashable:Any]) {
        if drawerController == nil {
            self.setupMMDrawerController()
        }
        print("USER_INFO:",userInfo)

    }

   

    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let installation = PFInstallation.current()
        installation?.setDeviceTokenFrom(deviceToken)
        installation?.saveInBackground()
        
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
    }
}

extension AppDelegate:UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler
        completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("notification435:",notification.request.content.userInfo)
        print("notification_presentation_options:",notification)
        //  MethodHelper.showUNNotification(title: "Test", message: "123")
        
        if let user = User.getCurrentUser(), user.allowNotifications {
            completionHandler([.alert, .sound])
        }else if !User.userExists(){
            completionHandler([.alert, .sound])
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("didReceiveNotificationResponse:",response.notification.request.content.userInfo)
        handleNotification(userInfo: response.notification.request.content.userInfo)
    }
    
    func handleNotification (userInfo: [AnyHashable:Any]) {
        print("handleNotification:", userInfo)
        handleTappedNotification(userInfo: userInfo)

    }
}
