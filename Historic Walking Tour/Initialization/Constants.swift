//
//  Constants.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 6/21/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import Foundation
import UIKit

let containerAppObjectId = "4C8vRDUgwW"
let colorLight = #colorLiteral(red: 1, green: 0.3607843137, blue: 0.1137254902, alpha: 1)
let colorPrimary = #colorLiteral(red: 0.2, green: 0.6901960784, blue: 0.937254902, alpha: 1)
let colorDark = #colorLiteral(red: 0.1450980392, green: 0.2901960784, blue: 0.6039215686, alpha: 1)
let upgradeViewHeight = CGFloat(60)

class ViewControllerIDs {
    static let MainViewController = "MainViewController"
    static let MenuViewController = "MenuViewController"
    static let LoginViewNavigationController = "LoginViewNavigationController"
    static let MainViewNavigationController = "MainViewNavigationController"
    static let LoginViewController = "LoginViewController"
    static let MenuViewNavigationController = "MenuViewNavigationController"
    static let RegisterViewController = "RegisterViewController"
    static let CityMainViewController = "CityMainViewController"
    static let TextViewController = "TextViewController"
    static let TourViewController = "TourViewController"
    static let EnjoyNearbyViewController = "EnjoyNearbyViewController"
    static let EnjoyNearbyDetailsViewController = "EnjoyNearbyDetailsViewController"
    static let PointDetailsViewController = "PointDetailsViewController"
    static let TourSitesViewController = "TourSitesViewController"
    static let CityHistoryViewController = "CityHistoryViewController"
    static let MoreViewController = "MoreViewController"
    static let ContactUsViewController = "ContactUsViewController"
    static let SettingsViewController = "SettingsViewController"
    static let CommentViewController = "CommentViewController"
    static let netGALAXYViewController = "netGALAXYViewController"
    static let SplashAdViewController = "SplashAdViewController"
}

class CellIDs {
    static let MenuTableViewCell = "MenuTableViewCell"
    static let PointTableViewCell = "PointTableViewCell"
    static let CityMenuTableViewCell = "CityMenuTableViewCell"
    static let EnjoyNearbyTableViewCell = "EnjoyNearbyTableViewCell"
    static let TourSitesTextTableViewCell = "TourSitesTextTableViewCell"
    static let TourSitesPointTableViewCell = "TourSitesPointTableViewCell"
    static let TourSitesBottomTableViewCell = "TourSitesBottomTableViewCell"
    static let MoreTableViewCell = "MoreTableViewCell"
    static let CommentTableViewCell = "CommentTableViewCell"
}

class PFColumns {
    static let objectId = "objectId"
    static let createdAt = "createdAt"
    
    class Installation {
        static let className = "Installation"
        static let objectId = "objectId"
        static let userObjectId = "userObjectId"
    }
    
    class User {
        static let className = "User"
        static let objectId = "objectId"
        static let accessLevel = "accessLevel"
        static let profilePicture = "profilePicture"
        static let username = "username"
        static let versionBuildInformation = "versionBuildInformation"
        static let email = "email"
        static let newUser = "newUser"
        static let firstName = "firstName"
        static let lastName = "lastName"
        static let purchasedObjectIds = "purchasedObjectIds"
        static let favoritePoints = "favoritePoints"
        static let allowNotifications = "allowNotifications"
        static let displayMiles = "displayMiles"
        static let permanentlyFree = "permanentlyFree"
        static let admin = "admin"
        static let freeListens = "freeListens"
    }
    class City {
        static let className = "City"
        static let title = "title"
        static let subtitle = "subtitle"
        static let imageFile = "imageFile"
        static let containerApp = "containerApp"
        static let productIdentifier = "productIdentifier"
        static let active = "active"
        static let howToUse = "howToUse"
        static let tourSitesText = "tourSitesText"
        static let tourSitesDay1Text = "tourSitesDay1Text"
        static let tourSitesDay2Text = "tourSitesDay2Text"
        static let tourSitesDay3Text = "tourSitesDay3Text"
        static let tourSitesDay4Text = "tourSitesDay4Text"
        static let tourSitesDay5Text = "tourSitesDay5Text"
        static let location = "location"
        static let historyText = "historyText"
        static let comingSoon = "comingSoon"
        static let hasRestrooms = "hasRestrooms"
        static let historyAudioUrl = "historyAudioUrl"
    }
    
    class ContainerApp {
        static let className = "ContainerApp"
    }
    
    class Point {
        static let className = "Point"
        static let title = "title"
        static let subtitle = "subtitle"
        static let imageFile = "imageFile"
        static let city = "city"
        static let location = "location"
        static let address = "address"
        static let restroom = "restroom"
        static let free = "free"
        static let tourSiteDay = "tourSiteDay"
        static let tourSiteOrder = "tourSiteOrder"
        static let audioUrl = "audioUrl"
        static let details = "details"
        static let adFile = "adFile"
        static let adUrl = "adUrl"
        static let isAdvertiser = "isAdvertiser"
        static let rulerAdFile = "rulerAdFile"
        static let rulerAdUrl = "rulerAdUrl"
        static let frenchAudioUrl = "frenchAudioUrl"
    }
    
    class EnjoyNearby {
        static let className = "EnjoyNearby"
        static let title = "title"
        static let subtitle = "subtitle"
        static let imageFile = "imageFile"
        static let city = "city"
        static let location = "location"
        static let address = "address"
        static let restroom = "restroom"
        static let category = "category"
        static let secondImageFile = "secondImageFile"
        static let phoneNumber = "phoneNumber"
        static let url = "url"
        static let isAdvertiser = "isAdvertiser"
    }
    
    class SliderImage {
        static let className = "SliderImage"
        static let imageFile = "imageFile"
        static let forObjectId = "forObjectId"
    }
    
    class Category {
        static let className = "Category"
        static let title = "title"
    }
   class Comment {
      static let className = "Comment"
      static let authorObjectId = "authorObjectId"
      static let commentText = "commentText"
      static let forObjectId = "forObjectId"
   }
    class SplashAd {
        static let className = "SplashAd"
        static let imageFile = "imageFile"
        static let active = "active"
        static let containerApp = "containerApp"
        static let url = "url"
    }
}

class PFConfigValues {
    static let privacyPolicy = "privacyPolicy"
    static let termsAndConditions = "termsAndConditions"
    static let faq = "faq"
    static let shareUrl = "shareUrl"
    static let aboutUs = "aboutUs"
    static let supportEmail = "supportEmail"
    static let websiteUrl = "websiteUrl"
    static let inProduction = "inProduction"
    static let franchiseOpportunities = "franchiseOpportunities"
    static let contactFormUrl = "contactFormUrl"
    static let contactUsQuestionType = "contactUsQuestionType"
    static let contactUsDeviceType = "contactUsDeviceType"
    static let contactUsOSType = "contactUsOSType"
    static let adColor = "adColor"
    static let premiumColor = "premiumColor"
    static let freeColor = "freeColor"
    static let enjoyNearbyColor = "enjoyNearbyColor"
    static let numberOfFreeListens = "numberOfFreeListens"
}

class NotificationNames {
    static let menuOpened = "menuOpened"
    static let pushNotificationReceived = "pushNotificationReceived"
    static let shouldHideCallout = "shouldHideCallout"
    static let loggedIn = "loggedIn"
    static let startedTimer = "startedTimer"
    static let purchasesRestored = "purchasesRestored"
}

class Facebook {
    static let facebookPermissions = ["public_profile","email"]
    static let facebookRequestParameters = ["fields": "id, name, first_name, last_name, picture.type(large), email"];
}

class UserDefaultKeys {
    static let savedEmail = "savedEmail"
    static let savedPassword = "savedPassword"
}
