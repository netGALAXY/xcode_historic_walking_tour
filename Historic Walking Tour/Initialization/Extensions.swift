//
//  Extensions.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 7/27/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import Foundation
import UIKit
import JTSImageViewController
import MapKit

extension UIViewController {
    func close () {
        if let navController = navigationController {
            navController.popViewController(animated: true)
        }else {
            dismiss(animated: true, completion: nil)
        }
    }
}


extension UILabel {
    func loadHtml (htmlString:String, completion: ((_ success:Bool)->Void)?) {
        let attrString = "<font face=\"AvenirNext-Regular\" size=\"4\">" + htmlString + "</font>"
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
            if let data = attrString.data(using: String.Encoding.unicode) {
                do {
                    
                    let attrString = try NSMutableAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType], documentAttributes: nil)
                    DispatchQueue.main.async {
                        self.attributedText = attrString
                        completion?(true)
                    }
                    
                }catch {
                    DispatchQueue.main.async {
                        completion?(false)
                    }
                }
            }else {
                DispatchQueue.main.async {
                    completion?(false)
                }
            }
        }
    }
    
}

extension CLLocation {
    func openInMaps(locationTitle:String?) {
        
        
        let regionDistance:CLLocationDistance = 10000
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinate, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        if let name = locationTitle {
            mapItem.name = name
        }
        mapItem.openInMaps(launchOptions: options)
    }
}

extension UIImageView{
    func addBlackGradientLayer(frame: CGRect){
        let gradient = CAGradientLayer()
        gradient.frame = frame
        gradient.colors = [ UIColor.clear.cgColor, UIColor.black.cgColor]
        gradient.locations = [0.5, 1]
        self.layer.addSublayer(gradient)
    }
    
    func displayImage (viewController:UIViewController) {
        guard let img = image else {return}
        let imageInfo = JTSImageInfo()
        imageInfo.image = img
        imageInfo.referenceRect = frame
        imageInfo.referenceView = viewController.view
        let imageView = JTSImageViewController(imageInfo: imageInfo, mode: .image, backgroundStyle: .blurred)
        imageView?.show(from: viewController, transition: .fromOriginalPosition)
    }
    
    
}

extension UITextField {
    func addLeftPadding () {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: frame.size.height))
        view.backgroundColor = UIColor.clear
        leftViewMode = .always
        leftView = view
    }
}

extension UIColor {
    convenience init(hex:String) {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            let oldColor = UIColor.gray
            self.init(cgColor: oldColor.cgColor)
        }else {
            var rgbValue:UInt32 = 0
            Scanner(string: cString).scanHexInt32(&rgbValue)
            
            let oldColor = UIColor(
                red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                alpha: CGFloat(1.0)
            )
            self.init(cgColor: oldColor.cgColor)
        }
        

    }
}
