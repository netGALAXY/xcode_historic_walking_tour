//
//  DrawerControllerSupport.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 6/21/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import Foundation
import MMDrawerController
import Parse

extension AppDelegate {
    func setupMMDrawerController () {
        AppDelegate.setMainViewAsCenterView()
        
    }
    
    class func setLoginViewAsCenterView () {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.lightContent, animated: true)
        let centerView = mainStoryboard.instantiateViewController(withIdentifier: ViewControllerIDs.LoginViewNavigationController) as! UINavigationController
        setCenterView(centerView: centerView)
    }
    
    class func setMainViewAsCenterView () {
        setParseInstallationObjectId()
        if User.userExists() {
            User.getCurrentUser()?.updateVersionBuildInformation()
        }
        PFUser.current()?.fetchInBackground(block: { (objectO, error) -> Void in
            if let _ = objectO {
                User.unsetCurrentUser()
            }
        })
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.lightContent, animated: true)
        let centerView = mainStoryboard.instantiateViewController(withIdentifier: ViewControllerIDs.MainViewNavigationController) as! UINavigationController
        setCenterView(centerView: centerView)
    }
    
    private class func setCenterView (centerView:UINavigationController) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if appDelegate.drawerController == nil {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let leftView   = mainStoryboard.instantiateViewController(withIdentifier: ViewControllerIDs.MenuViewNavigationController) as! UINavigationController
            appDelegate.drawerController = MMDrawerController(center: centerView, leftDrawerViewController: leftView)
            appDelegate.drawerController.showsShadow = false
            appDelegate.drawerController.setMaximumLeftDrawerWidth(250, animated: true, completion: nil)
            // appDelegate.drawerController.openDrawerGestureModeMask = .all
            appDelegate.drawerController.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.all
            
            appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
            appDelegate.window?.rootViewController = appDelegate.drawerController
            appDelegate.window?.makeKeyAndVisible()
        }else {
            appDelegate.drawerController.setCenterView(centerView, withCloseAnimation: true, completion: nil)
        }
    }
    
    class func toggleMMDrawer () {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.endEditing(true)
        if appDelegate.drawerController.visibleLeftDrawerWidth <= 0 {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationNames.menuOpened), object: nil)
        }
        appDelegate.drawerController.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }
    
    class func setCenterNavigationController (storyboardID:String) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.drawerController.setCenterView(mainStoryboard.instantiateViewController(withIdentifier: storyboardID), withCloseAnimation: false, completion: nil)
    }
}
