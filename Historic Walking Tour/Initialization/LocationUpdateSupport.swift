//
//  LocationUpdateSupport.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 7/7/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

extension AppDelegate:CLLocationManagerDelegate {
    func setupLocationManager () {
        //  Converted with Swiftify v1.0.6242 - https://objectivec2swift.com/
        locationManager = CLLocationManager()
        //set delegate
        locationManager.delegate = self
        // This is the most important property to set for the manager. It ultimately determines how the manager will
        // attempt to acquire location and thus, the amount of power that will be consumed.
        
        locationManager.desiredAccuracy = 45
        locationManager.distanceFilter = 100
        // Once configured, the location manager must be "started".
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {return}
        currentLocation = location
    }
}
