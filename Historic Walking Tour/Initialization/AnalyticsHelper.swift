//
//  AnalyticsHelper.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 11/1/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import Foundation
import Firebase

class AnalyticsHelper {
    class func logCitySelectEvent (city:City) {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID: city.title as NSObject,
            AnalyticsParameterItemName: city.objectId as NSObject,
            AnalyticsParameterContentType: "city" as NSObject
            ])
    }
    
    class func logCityPurchasedEvent (city:City) {
        Analytics.logEvent(AnalyticsEventEcommercePurchase, parameters: [
            AnalyticsParameterItemID: city.title as NSObject,
            AnalyticsParameterItemName: city.objectId as NSObject,
            AnalyticsParameterContentType: "city" as NSObject
            ])
    }
    
    class func logPointSelectEvent (point:Point) {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID: point.title as NSObject,
            AnalyticsParameterItemName: point.objectId as NSObject,
            AnalyticsParameterValue: point.objectId as NSObject,
            AnalyticsParameterContentType: "point" as NSObject
            ])
    }
    
    class func logEnjoyPointSelectEvent (enjoyNearby:EnjoyNearby) {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID: enjoyNearby.title as NSObject,
            AnalyticsParameterItemName: enjoyNearby.objectId as NSObject,
            AnalyticsParameterValue: enjoyNearby.objectId as NSObject,
            AnalyticsParameterContentType: "enjoyPoint" as NSObject
            ])
    }
}
