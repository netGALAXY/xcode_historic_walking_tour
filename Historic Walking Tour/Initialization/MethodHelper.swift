//
//  MethodHelper.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 6/21/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import Foundation
import UIKit
import Parse
import JGProgressHUD
import IAPHelper
import CoreLocation
import UserNotifications

class MethodHelper {
    private static var hud = JGProgressHUD(style: JGProgressHUDStyle.dark)
    
    class func showHudWithMessage (message:String, view:UIView) {
        self.hud?.textLabel.text = message
        self.hud?.show(in: view)
    }
    
    class func hideHUD () {
        self.hud?.dismiss()
    }
    
    class func getVerisionAndBuildString () -> String {
        return "Version: \(Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString")!) - Build: \(Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion")!)"
    }
    
    class func showAlert (title:String, message:String) {
        guard var topVC = UIApplication.shared.keyWindow?.rootViewController else {return}
        while topVC.presentedViewController != nil {
            if let vc = topVC.presentedViewController {
                topVC = vc
            }
        }
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        topVC.present(alertController, animated: true, completion: nil)
    }
    
    class func setNeedsUpdate (needsUpdate:Bool) {
        PFCloud.callFunction(inBackground: "setNeedsToUpdate", withParameters: ["needsToUpdate":NSNumber(booleanLiteral: needsUpdate)] as [String : Any]) { (response, error) in
            print("CALLED_activateNeedsRefresh:",response,error)
        }
    }
    
    class func addNoneLabelToView (width: Float, view:UIView, belowView:UIView, text:String) -> UILabel {
        let label = UILabel(frame: CGRect(x: 8, y: Int(belowView.frame.origin.y + belowView.frame.size.height + 8), width: Int(width), height: 80))
        label.text = text
        label.numberOfLines = 0
        label.isUserInteractionEnabled = true
        label.textAlignment = .center
        view.addSubview(label)
        label.font = UIFont(name: "AvenirNext-Regular", size: 14.0)
        label.isHidden = true
        let xConstraint = NSLayoutConstraint(item: label, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
        let yConstraint = NSLayoutConstraint(item: label, attribute: .top, relatedBy: .equal, toItem: belowView, attribute: .bottom, multiplier: 1, constant: 8)
        let widthConstraint = NSLayoutConstraint(item: label, attribute: .width, relatedBy: .equal, toItem: view, attribute: .width, multiplier: 1, constant: -16)
        let heightConstraint = NSLayoutConstraint(item: label, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40)
        view.addConstraints([xConstraint, yConstraint, widthConstraint, heightConstraint])
        return label
    }
    
    class func getMilesBetweenLocations (location1:CLLocation, location2:CLLocation) -> Float {
        let meters = location1.distance(from: location2)
        return Float(meters / 1609.34)
    }
    
    class func startIAPHelper (productIds:[String]) {
        if productIds.count > 0 {
            if IAPShare.sharedHelper().iap == nil {
                let productIdentifiers = Set(productIds)
                IAPShare.sharedHelper().iap = IAPHelper(productIdentifiers: productIdentifiers)
            }
            IAPShare.sharedHelper().iap.production = Config.current().inProduction
            IAPShare.sharedHelper().iap.requestProducts { (request, response) in
                print("IAP_REQUEST_PRODUCTS:",IAPShare.sharedHelper().iap.products)
            }
        }
    }
    
    class func getCurrentLocation (completion:@escaping (_ locationO:CLLocation?)->Void) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let location = appDelegate.currentLocation {
            completion(location)
        }else {
            PFGeoPoint.geoPointForCurrentLocation(inBackground: { (geoPointO, error) -> Void in
                if let geoPoint = geoPointO {
                    let location = CLLocation(latitude: geoPoint.latitude, longitude: geoPoint.longitude)
                    appDelegate.currentLocation = location
                }
                completion(appDelegate.currentLocation)
                
            })
        }
        
    }
    
    class func getCityAndStateFromLocation (_ location:CLLocation, completion:@escaping (_ city:String?, _ state:String?)->Void) {
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(location) { (placeMarksO, errorO) -> Void in
            if let placeMarks = placeMarksO {
                if let placeMark = placeMarks.first {
                    
                    completion(placeMark.locality, placeMark.administrativeArea)
                }else {
                    completion(nil, nil)
                }
            }
        }
    }
    
    class func showUNNotification (title:String, message:String) {
        let center = UNUserNotificationCenter.current()
        
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = message
        content.sound = UNNotificationSound(named: "GeneralAlert.caf")
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        center.add(request)
    }
    
    class func call (phoneNumebr:String?) {
        if let number = phoneNumebr, let url = URL(string: "tel://\(number)"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
}

extension UIView {
   
   func bindFrameToSuperviewBounds() {
      if superview == nil {
         return
      }
      
      guard let superview = self.superview else {
         print("Error! `superview` was nil – call `addSubview(view: UIView)` before calling `bindFrameToSuperviewBounds()` to fix this.")
         return
      }
      
      self.translatesAutoresizingMaskIntoConstraints = false
      let x = NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: superview, attribute: .centerX, multiplier: 1, constant: 0)
      let y = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: superview, attribute: .centerY, multiplier: 1, constant: 0)
      let w = NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: superview, attribute: .width, multiplier: 1, constant: 0)
      let h = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: superview, attribute: .height, multiplier: 1, constant: 0)
      
      
      superview.addConstraints([x,y,w,h])
   }
   
   func circle () {
      layer.cornerRadius = frame.size.width / 2.0
      clipsToBounds = true
   }
   
   func addShadow() {
      self.layer.masksToBounds = false
      self.layer.shadowColor = UIColor.black.cgColor
      self.layer.shadowOpacity = 0.5
      self.layer.shouldRasterize = true;
      self.layer.rasterizationScale = UIScreen.main.scale;
      self.layer.shadowOffset = CGSize(width: 1, height: 1)
      self.layer.shadowRadius = 1
      
   }
   
    
    
   
}


