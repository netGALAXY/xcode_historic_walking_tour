//
//  MoreTableViewCell.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 7/25/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit

class MoreTableViewCell: UITableViewCell {
    var menuItem:MoreViewController.MenuItem!
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func populate (menuItem:MoreViewController.MenuItem, city:City) {
        self.menuItem = menuItem
        if menuItem == .Purchase {
            titleLabel.text = "Purchase all " + city.title + " locations"
            iconImageView.image = #imageLiteral(resourceName: "download")
        }else if menuItem == .Help {
            titleLabel.text = "Help"
            iconImageView.image = #imageLiteral(resourceName: "help")
        } else if menuItem == .ContactUs {
            titleLabel.text = "Contact Us"
            iconImageView.image = #imageLiteral(resourceName: "contact-us")
        }else if menuItem == .Favorites {
            titleLabel.text = "Favorite Spots"
            iconImageView.image = #imageLiteral(resourceName: "favorite-spot")
        }else if menuItem == .FranchiseOpportunities {
            titleLabel.text = "Franchise Opportunities"
            iconImageView.image = #imageLiteral(resourceName: "franchise")
        }else if menuItem == .Settings {
            titleLabel.text = "Settings"
            iconImageView.image = #imageLiteral(resourceName: "settings")
        }else if menuItem == .PublicRestrooms {
            titleLabel.text = "Public Restrooms"
            iconImageView.image = #imageLiteral(resourceName: "restroom")
        }
    }

}
