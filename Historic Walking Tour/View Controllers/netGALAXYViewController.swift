//
//  netGALAXYViewController.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 8/18/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit

class netGALAXYViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backButtonPressed (sender:UIButton) {
        close()
    }
    
    class func display(viewController:UIViewController, navigationController:UINavigationController?) {
        guard let nextView = viewController.storyboard?.instantiateViewController(withIdentifier: ViewControllerIDs.netGALAXYViewController) as? netGALAXYViewController else {return}
        if let navController = navigationController {
            navController.pushViewController(nextView, animated: true)
        }else {
            viewController.present(nextView, animated: true, completion: nil)
        }
        
    }

}
