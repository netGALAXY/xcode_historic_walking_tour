//
//  TextViewController.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 7/5/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit

class TextViewController: UIViewController {
    var type:TextType!
    var city:City?
    var url:String?
    
    @IBOutlet weak var upgradeViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var upgradeView: UIView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        titleLabel.text = type.rawValue
        webView.delegate = self
        
        if type == TextType.TermsAndConditions, let terms = Config.current().termsAndConditions {
            getTextFromString(string: terms)
        }else if type == TextType.PrivacyPolicy, let privacy = Config.current().privacyPolicy {
            getTextFromString(string: privacy)
        }else if type == .FAQ {
            getTextFromString(string: Config.current().faq)
        }else if type == .About {
            getTextFromString(string: Config.current().aboutUs)
        }else if type == .HowToUse, let c = city {
            getTextFromString(string: c.howToUse)
            setUpgradeView()
        }else if type == .FranchiseOpportunities {
            getTextFromString(string: Config.current().franchiseOpportunities)
        }else if type == .Website, let urlString = url, let u = URL(string: urlString) {
            webView.loadRequest(URLRequest(url: u))
        }
      webView.backgroundColor = UIColor.white
    }
    
    func setUpgradeView () {
        if let c = city {
            if !c.isPurchased() {
                upgradeViewHeightConstraint.constant = upgradeViewHeight
                upgradeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(TourSitesViewController.upgradeViewTapped(_:))))
            }else {
                upgradeViewHeightConstraint.constant = 0
            }
            view.layoutIfNeeded()
            
        }
    }
    
    @objc func upgradeViewTapped (_ tap:UITapGestureRecognizer) {
        MethodHelper.showHudWithMessage(message: "Loading...", view: view)
        city?.buy(completion: { (success) in
            MethodHelper.hideHUD()
            if success {
                self.setUpgradeView()
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        close()
    }
    
    class func display(viewController:UIViewController, navigationController:UINavigationController?, type:TextType, city:City?, url:String?) {
        guard let nextView = viewController.storyboard?.instantiateViewController(withIdentifier: ViewControllerIDs.TextViewController) as? TextViewController else {return}
        nextView.type = type
        nextView.city = city
        nextView.url = url
        if let navController = navigationController {
            navController.pushViewController(nextView, animated: true)
        }else {
            viewController.present(nextView, animated: true, completion: nil)
        }
        
    }
    
    func getTextFromString (string:String) {
        MethodHelper.showHudWithMessage(message: "Loading...", view: self.view)
        self.webView.loadHTMLString(string, baseURL: nil)

        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
            DispatchQueue.main.async {
                MethodHelper.hideHUD()
            }
        }
        
    }
    
    enum TextType:String {
        case TermsAndConditions = "Terms and Conditions", PrivacyPolicy = "Privacy Policy", About = "About", FAQ = "FAQ", HowToUse = "How to Use", FranchiseOpportunities = "Franchise Opportunities", Website = "Website"
    }
    
}

extension TextViewController:UIWebViewDelegate {
    func webViewDidStartLoad(_ webView: UIWebView) {
        MethodHelper.showHudWithMessage(message: "Loading...", view: webView)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        MethodHelper.hideHUD()
    }
}

