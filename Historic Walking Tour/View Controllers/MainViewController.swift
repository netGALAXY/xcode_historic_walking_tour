//
//  MainViewController.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 6/21/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit
import IAPHelper

class MainViewController: UIViewController {
   var sectionImageViewX :CGFloat = 0
   var previousView:UIView!
   var leadingXConstraint:NSLayoutConstraint!
   var currentIndex = 0
    var cities:[City] = []
   var sliderViews:[SliderContainerView] = []
   
   @IBOutlet weak var gestureView: UIView!
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(MainViewController.gestureViewSwiped(_:)))
      swipeRight.direction = UISwipeGestureRecognizerDirection.right
      self.gestureView.addGestureRecognizer(swipeRight)
      let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(MainViewController.gestureViewSwiped(_:)))
      swipeLeft.direction = UISwipeGestureRecognizerDirection.left
      self.gestureView.addGestureRecognizer(swipeLeft)
      
      self.gestureView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(MainViewController.gestureViewTapped(_:))))
        
        City.getAllCities { (cities) in
            self.cities = cities
            var productIds:[String] =  []
            for city in cities {
                if let productId = city.productIdentifier {
                    productIds.append(productId)
                }
                self.sliderViews.append(SliderContainerView(city:city, viewController: self, delegate: self))
            }
            MethodHelper.startIAPHelper(productIds: productIds)
            
        }
        
        SplashAd.get { (splashAd) in
            if let splashAd = splashAd {
                SplashAdViewController.display(viewController: self, navigationController: nil, splashAd: splashAd)
            }
        }
    }
    

   
   override func viewDidAppear(_ animated: Bool) {
      super.viewDidAppear(animated)
    
   }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   @objc func gestureViewTapped (_ tap:UITapGestureRecognizer) {
      if sliderViews.count > currentIndex {
         let sectionView = sliderViews[currentIndex]
         print("SECTION_VIEW:",sectionView)
      }
      
   }
   
   @objc func gestureViewSwiped (_ tap:UISwipeGestureRecognizer) {
      print("handleSwipe0")
      switch tap.direction {
      case UISwipeGestureRecognizerDirection.right:
         handleSwipe(moveLeft: false)
         break
      case UISwipeGestureRecognizerDirection.left:
         handleSwipe(moveLeft: true)
         break
      default:
         break
      }
   }
    
    @IBAction func menuButtonPressed(_ sender: UIButton) {
        AppDelegate.toggleMMDrawer()
    }
    
   func handleSwipe (moveLeft:Bool) {
      view.layoutIfNeeded()
      print("handleSwipe1")
      if moveLeft {
         if currentIndex < cities.count - 1 {
            print("handleSwipe2")
            leadingXConstraint.constant = leadingXConstraint.constant - (UIScreen.main.bounds.size.width - 76)
            currentIndex += 1
         }
      }else {
         if currentIndex > 0 {
            print("handleSwipe3")
            leadingXConstraint.constant = leadingXConstraint.constant + (UIScreen.main.bounds.size.width - 76)
            currentIndex -= 1
         }
      }
      UIView.animate(withDuration: 0.5) {
         self.view.layoutIfNeeded()
      }
      
   }

}

extension MainViewController:SliderContainerViewDelegate {
    func clickedExplore(forCity:City) {
        if let _ = User.getCurrentUser() {
            CityMainViewController.display(forCity: forCity, viewController: self, navigationController: navigationController)
        }else {
            LoginViewController.display(viewController: self, navigationController: nil)
        }
    }
}
