//
//  MenuViewController.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 6/21/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit
import IAPHelper

class MenuViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
        self.navigationController?.view.layoutSubviews()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    class MenuTitles {
        static let about = "About Historic Walking Tour"
        static let shareThisApp = "Share this App"
        static let restorePurchases = "Restore Purchases"
        static let settings = "Settings"
        static let logout = "Logout"
        static let login = "Login"
        static let profile = "Profile"
        static let tutorial = "Tutorial"
    }
    
}

extension MenuViewController:UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIDs.MenuTableViewCell, for: indexPath) as! MenuTableViewCell
        
        if indexPath.row == 0 {
            cell.titleLabel.text = MenuTitles.shareThisApp
        }else if indexPath.row == 1 {
            cell.titleLabel.text = MenuTitles.settings
        }else if indexPath.row == 2 {
            cell.titleLabel.text = MenuTitles.restorePurchases
        }else if indexPath.row == 3 {
            if let _ = User.getCurrentUser() {
                cell.titleLabel.text = MenuTitles.logout
            }else {
                cell.titleLabel.text = MenuTitles.login
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let centerViewNavigationController = (UIApplication.shared.delegate as! AppDelegate).drawerController.centerViewController as! UINavigationController
        guard let centerViewController = centerViewNavigationController.visibleViewController else {return}
        let cell = tableView.cellForRow(at: indexPath) as! MenuTableViewCell
        let title = cell.titleLabel.text!
        
        if title == MenuTitles.logout {
            if let centerView = centerViewController.view {
                MethodHelper.showHudWithMessage(message: "Logging out...", view: centerView)
            }
            User.logOut({ (success) in
                MethodHelper.hideHUD()
                if success {
                    self.tableView.reloadData()
                }else {
                    MethodHelper.showAlert(title: "Error", message: "There was an error logging out. Please try again.")
                }
            })
        }else if title == MenuTitles.login {
            LoginViewController.display(viewController: centerViewController, navigationController: nil)
        } else if title == MenuTitles.restorePurchases && IAPShare.sharedHelper().iap != nil {
            if let centerView = centerViewController.view {
                MethodHelper.showHudWithMessage(message: "Restoring Purchases...", view: centerView)
            }
            restorePurchases()
            
        }else if title == MenuTitles.shareThisApp {
            share()
        }else if title == MenuTitles.about {
            TextViewController.display(viewController: centerViewController, navigationController: centerViewNavigationController, type: .About, city: nil, url: nil)
        }else if title == MenuTitles.settings {
            SettingsViewController.display(viewController: centerViewController, navigationController: centerViewNavigationController)
        }
        
        AppDelegate.toggleMMDrawer()
    }
    
    private func restorePurchases () {
        IAPShare.sharedHelper().iap.restoreProducts(completion: { (queue, error) in
            if error == nil {
                User.refresh (completion: { (success) in
                    MethodHelper.hideHUD()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationNames.purchasesRestored), object: nil)
                    MethodHelper.showAlert(title: "Success", message: "Your purchases have been restored successfully!")
                })
            }else {
                MethodHelper.hideHUD()
                MethodHelper.showAlert(title: "Error", message: "There was an error restoring your purchases. Please try again.")
            }
        })
    }
    
    func share () {
        var items:[Any] = []
        items.append("Check out this cool new app called Historic Walking Tour! Available for download today! - \(Config.current().shareUrl)")
        items.append(#imageLiteral(resourceName: "app_icon_large"))
        let activityController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(activityController, animated: true, completion: nil)
    }
    
}
