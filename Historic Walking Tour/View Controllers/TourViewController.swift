//
//  TourViewController.swift
//  Historic Walking Tour
//
//  Created by Charlie  on 7/6/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit
import MapKit

class TourViewController: UIViewController {
   var city:City!
    var points:[Point] = []
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableViewXConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapView: MKMapView!
    var pointDictionary:[String:Point] = [:]
    var forType:ForType!
    var noneLabel:UILabel?
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var listMapButton: UIButton!
    @IBOutlet weak var upgradeViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var upgradeView: UIView!
    private var displayMapFirst = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        MethodHelper.showHudWithMessage(message: "Loading...", view: self.view)
        
        titleLabel.text = city.title + ", " + city.subtitle
        if forType == .tourSites {
            Point.getPoints(forCity: city, sortByDistance: true) { (points) in
                self.loadPoints(points: points)
            }
            setUpgradeView()

        }else if forType == .publicRestrooms {
            Point.getRestrooms(forCity: city, completion: { (points) in
                self.loadPoints(points: points)
            })
        }
        
        if displayMapFirst {
            toggleMap(sender: listMapButton, displayMap: true, animate: false)
        }
        
    }
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.contentInset = UIEdgeInsetsMake(0, 0, 200, 0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
    }
   
   
    
    private func loadPoints (points:[Point]) {
        self.points = points
        self.tableView.reloadData()
        self.loadMapView()
        MethodHelper.hideHUD()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
      if forType == ForType.favorites {
        self.noneLabel = MethodHelper.addNoneLabelToView(width: Float(UIScreen.main.bounds.size.width - 16), view: self.view, belowView: self.headerView, text: "You have not added any favorite spots")
        self.noneLabel?.isHidden = true
        guard let user = User.getCurrentUser() else {return}
         Point.getFavoritePoints(forCity: city, forUser: user, completion: { (points) in
            self.loadPoints(points: points)
            if points.count < 1 {
                self.noneLabel?.isHidden = false
            }else {
                self.noneLabel?.isHidden = true
            }
         })
      }
        
        
    }
    
    func setUpgradeView () {
        if let c = city {
            if !c.isPurchased() {
                upgradeViewHeightConstraint.constant = upgradeViewHeight
                upgradeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(TourSitesViewController.upgradeViewTapped(_:))))
            }else {
                upgradeViewHeightConstraint.constant = 0
            }
            view.layoutIfNeeded()
            
        }
    }
    
    @objc func upgradeViewTapped (_ tap:UITapGestureRecognizer) {
        MethodHelper.showHudWithMessage(message: "Loading...", view: view)
        city?.buy(completion: { (success) in
            MethodHelper.hideHUD()
            if success {
                self.setUpgradeView()
                self.tableView.reloadData()
            }
        })
    }
    
    func loadMapView () {
        for point in points {
            let annotation = MKPointAnnotation()
            annotation.title = point.title
            annotation.subtitle = point.address
            annotation.coordinate = point.location.coordinate
            pointDictionary[point.title] = point
            mapView.addAnnotation(annotation)
        }
        mapView.showAnnotations(mapView.annotations, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    class func display(viewController:UIViewController, navigationController:UINavigationController?, city:City, forType:ForType, displayMapFirst:Bool) {
      guard let nextView = viewController.storyboard?.instantiateViewController(withIdentifier: ViewControllerIDs.TourViewController) as? TourViewController else {return}
      nextView.city = city
        nextView.forType = forType
        nextView.displayMapFirst = displayMapFirst
      if let navController = navigationController {
         navController.pushViewController(nextView, animated: true)
      }else {
         viewController.present(nextView, animated: true, completion: nil)
      }
      
   }

    @IBAction func listMapButtonPressed(_ sender: UIButton) {
        toggleMap(sender: sender, displayMap: tableViewXConstraint.constant >= 0, animate: true)
    }
    
    private func toggleMap (sender: UIButton, displayMap:Bool, animate:Bool) {
        if displayMap {
            //switching to map
            tableViewXConstraint.constant = -tableView.frame.size.width
            sender.setImage(#imageLiteral(resourceName: "list"), for: .normal)
        }else {
            //switching to list
            tableViewXConstraint.constant = 0
            sender.setImage(#imageLiteral(resourceName: "map_pin"), for: .normal)
        }

        UIView.animate(withDuration: animate ? 0.2 : 0.0) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        tableView.isHidden = true
        close()
    }
    
    func getNumberOfFreePoints () -> Int {
        var count = 0
        for point in points {
            if point.free {
                count += 1
            }
        }
        return count
    }
    
}

extension TourViewController:UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if forType == ForType.tourSites {
            return points.count + 1
        }else {
            return points.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIDs.PointTableViewCell, for: indexPath) as? PointTableViewCell else {return UITableViewCell()}
        if forType == .tourSites {
            if city.isPurchased() {
                if indexPath.row == 0 {
                    cell.populate(forPoint: nil, forCity: city)
                }else {
                    cell.populate(forPoint: points[indexPath.row - 1], forCity: nil)
                }
            }else {
                if indexPath.row == getNumberOfFreePoints() {
                    cell.populate(forPoint: nil, forCity: city)
                }else if indexPath.row > getNumberOfFreePoints() + 1 {
                    cell.populate(forPoint: points[indexPath.row - 1], forCity: nil)
                }else {
                    cell.populate(forPoint: points[indexPath.row], forCity: nil)
                }
            }
        }else {
            cell.populate(forPoint: points[indexPath.row], forCity: nil)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if forType == .tourSites {
            if city.isPurchased() {
                if indexPath.row == 0 {
                    CityHistoryViewController.display(viewController: self, navigationController: navigationController, city: city)
                }else {
                    PointDetailsViewController.display(viewController: self, navigationController: navigationController, point: points[indexPath.row - 1])
                }
            }else {
                
                if indexPath.row == getNumberOfFreePoints() + 1 || indexPath.row == getNumberOfFreePoints() {
                    self.displayPremiumDialog(continueToPoint: nil)
                }else if indexPath.row > getNumberOfFreePoints() + 1 {
                    PointDetailsViewController.display(viewController: self, navigationController: navigationController, point: points[indexPath.row - 2])
                }else {
                    guard let user = User.getCurrentUser() else {return}
                    if user.isUnderFreeViews() {
                        PointDetailsViewController.display(viewController: self, navigationController: navigationController, point: points[indexPath.row])
                    }else {
                        displayPremiumDialog(continueToPoint: points[indexPath.row])
                    }
                }
            }
        }else {
            PointDetailsViewController.display(viewController: self, navigationController: navigationController, point: points[indexPath.row])
        }
    }
    
    private func displayPremiumDialog (continueToPoint:Point?) {
        let alertController = UIAlertController(title: "Premium", message: "Would you like to purchase " + city.title + "?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "No, thanks", style: .default, handler: {action in
            if let point = continueToPoint {
                PointDetailsViewController.display(viewController: self, navigationController: self.navigationController, point: point)
            }
        }))
        alertController.addAction(UIAlertAction(title: "Yes!", style: .default, handler: { (action) in
            MethodHelper.showHudWithMessage(message: "Loading...", view: self.view)
            self.city.buy(completion: { (success) in
                MethodHelper.hideHUD()
                if success {
                    print("PURCHASED_PRODUCT")
                    self.tableView.reloadData()
                }else {
                    MethodHelper.showAlert(title: "Error", message: "There was an error purchasing this city. Please try again.")
                }
            })
        }))
        self.present(alertController, animated: true, completion: nil)
    }
}

extension TourViewController:MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        if let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "pin") {
            annotationView.annotation = annotation
            return annotationView
        } else {
            let annotationView = MKPinAnnotationView(annotation:annotation, reuseIdentifier:"pin")
            annotationView.isEnabled = true
            annotationView.canShowCallout = true
            if let t = annotation.title, let title = t, let point = pointDictionary[title], point.isAdvertiser {
                annotationView.pinTintColor = Config.current().adColor
            }else if let t = annotation.title, let title = t, let point = pointDictionary[title], !point.currentUserHasAccess()  {
                annotationView.pinTintColor = Config.current().premiumColor
            }else {
                annotationView.pinTintColor = Config.current().freeColor
            }

            annotationView.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            return annotationView
        }
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        guard let t = view.annotation?.title, let title = t, let point = pointDictionary[title] else {return}
        PointDetailsViewController.display(viewController: self, navigationController: navigationController, point: point)
    }
}

enum ForType:String {
    case tourSites = "Tour Sites"
    case favorites = "Favorite"
    case publicRestrooms = "Public Restrooms"
}

