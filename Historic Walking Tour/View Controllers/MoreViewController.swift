//
//  MoreViewController.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 7/25/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit

class MoreViewController: UIViewController {
    var city:City!
    var menuItems = [MenuItem.Purchase, MenuItem.Help, MenuItem.PublicRestrooms, MenuItem.Favorites, MenuItem.FranchiseOpportunities, MenuItem.ContactUs, MenuItem.Settings]
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonPressed (sender:UIButton) {
        close()
    }
    
    class func display(viewController:UIViewController, navigationController:UINavigationController?, city:City) {
        guard let nextView = viewController.storyboard?.instantiateViewController(withIdentifier: ViewControllerIDs.MoreViewController) as? MoreViewController else {return}
        nextView.city = city
        if let navController = navigationController {
            navController.pushViewController(nextView, animated: true)
        }else {
            viewController.present(nextView, animated: true, completion: nil)
        }
        
    }

}

extension MoreViewController:UITableViewDelegate,UITableViewDataSource {

    enum MenuItem {
        case Purchase, Help, PublicRestrooms, Favorites, FranchiseOpportunities, ContactUs, Settings
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount = 7
        if city.isPurchased() {
            rowCount -= 1
        }
        if !city.hasRestrooms {
            rowCount -= 1
        }
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIDs.MoreTableViewCell, for: indexPath) as? MoreTableViewCell else {return UITableViewCell()}
        
        var row = indexPath.row
        if city.isPurchased() {
            row += 1
        }
        if !city.hasRestrooms && indexPath.row > 0 {
            row += 1
        }
        cell.populate(menuItem: menuItems[row], city: city)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? MoreTableViewCell else {return}
        if cell.menuItem == .Purchase {
            purchasePressed()
        }else if cell.menuItem == .Help {
            TextViewController.display(viewController: self, navigationController: navigationController, type: .FAQ, city: city, url: nil)
        } else if cell.menuItem == .ContactUs {
            ContactUsViewController.display(viewController: self)
        }else if cell.menuItem == .Favorites {
            TourViewController.display(viewController: self, navigationController: navigationController, city: city, forType: .favorites, displayMapFirst: true)
        }else if cell.menuItem == .FranchiseOpportunities {
            TextViewController.display(viewController: self, navigationController: navigationController, type: .FranchiseOpportunities, city: city, url: nil)
        }else if cell.menuItem == .Settings {
            SettingsViewController.display(viewController: self, navigationController: navigationController)
        }else if cell.menuItem == .PublicRestrooms {
            TourViewController.display(viewController: self, navigationController: navigationController, city: city, forType: .publicRestrooms, displayMapFirst: true)
        }
    }
    
    func purchasePressed () {
        let alertController = UIAlertController(title: "Purchase " + city.title, message: "You are about to purchase \(city.title) and unlock all tour locations. Would you like to purchase " + city.title + "?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "No, thanks", style: .default, handler: nil))
        alertController.addAction(UIAlertAction(title: "Yes!", style: .default, handler: { (action) in
            MethodHelper.showHudWithMessage(message: "Loading...", view: self.view)
            self.city.buy(completion: { (success) in
                MethodHelper.hideHUD()
                if success {
                    print("PURCHASED_PRODUCT")
                    self.tableView.reloadData()
                }else {
                    MethodHelper.showAlert(title: "Error", message: "There was an error purchasing this city. Please try again.")
                }
            })
        }))
        present(alertController, animated: true, completion: nil)
    }
}
