//
//  CityHistoryViewController.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 7/25/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit

class CityHistoryViewController: UIViewController {
    var city:City!
    @IBOutlet weak var historyLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var cityIamgeView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var mediaPlayerWebView: UIWebView!
    @IBOutlet weak var mediaPlayerHeightConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadHistory()
        city.getImage { (imageO) in
            if let image = imageO {
                self.cityIamgeView.image = image
            }
        }
        titleLabel.text = "History of " + city.title
        setupMediaPlayer()
    }
    
    func setupMediaPlayer () {
        mediaPlayerWebView.scrollView.isScrollEnabled = false
        mediaPlayerWebView.backgroundColor = UIColor.white
        if let audioHTML = getAudioHtml() {
            mediaPlayerWebView.loadHTMLString(audioHTML, baseURL: nil)
        }else {
            mediaPlayerHeightConstraint.constant = 0
            view.layoutIfNeeded()
        }
    }
    
    private func getAudioHtml () -> String? {
        guard let audioUrl = city.historyAudioUrl else {return nil}
        return "<!DOCTYPE html> <html> <head> <style> #audioBar { padding: 0; display: block; margin: 0 auto; max-height: 100%; width: 100%; } </style> </head> <body><audio id=\"audioBar\" controls><source src=\"\(audioUrl)\" type=\"audio/mpeg\" controls=\"1\">Your browser does not support the audio element.</audio> </body> </html>"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        close()
    }
    
    class func display(viewController:UIViewController, navigationController:UINavigationController?, city:City) {
        guard let nextView = viewController.storyboard?.instantiateViewController(withIdentifier: ViewControllerIDs.CityHistoryViewController) as? CityHistoryViewController else {return}
        nextView.city = city
        if let navController = navigationController {
            navController.pushViewController(nextView, animated: true)
        }else {
            viewController.present(nextView, animated: true, completion: nil)
        }
        
    }
    
    func loadHistory () {
        if let html = getHistoryHtml() {
            MethodHelper.showHudWithMessage(message: "Loading...", view: self.view)
            var blankString = ""
            for _ in 0..<300 {
                blankString += "-"
            }
            self.historyLabel.text = html
            self.historyLabel.textColor = UIColor.clear
            self.historyLabel.sizeToFit()
            historyLabel.loadHtml(htmlString: html, completion: { (success) in
                
                MethodHelper.hideHUD()
            })
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.size.width, height: historyLabel.frame.origin.y + historyLabel.frame.size.height)
        scrollView.isScrollEnabled = true
    }
    
    func getHistoryHtml ()->String? {
        guard let history = city.historyText else {return nil}
        return "<font face=\"AvenirNext-Regular\" size=\"5\">" + history + "</font>"
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
