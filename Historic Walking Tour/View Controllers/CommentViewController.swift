//
//  CommentViewController.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 7/27/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding

class CommentViewController: UIViewController {
    var forObjectId = ""
    
    @IBOutlet weak var noCommentsImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
   @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var textField: UITextField!
   var comments:[Comment] = []
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        textField.addLeftPadding()
        self.automaticallyAdjustsScrollViewInsets = false
      tableView.rowHeight = UITableViewAutomaticDimension

      scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: -8, right: 0)
      
        scrollView.scrollIndicatorInsets = UIEdgeInsets.zero;
        
        scrollView.contentOffset = CGPoint(x: 0.0, y: 0.0)
        NotificationCenter.default.addObserver(self, selector: #selector(CommentViewController.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
      
      Comment.getComments(forObjectId: forObjectId) { (comments) in
         self.comments = comments
         self.tableView.reloadData()
        if comments.count > 0 {
            self.tableView.scrollToRow(at: IndexPath(row: 0, section: comments.count - 1), at: UITableViewScrollPosition.top, animated: true)
            
        }else {
            self.noCommentsImageView.isHidden = false
        }
      }
    }
    
    @objc func keyboardWillHide (notification:NSNotification) {
        print("Keyboard_gone")
      scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: -10, right: 0)
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonPressed (sender:UIButton) {
        close()
    }
    
   @IBAction func sendButtonPressed(_ sender: UIButton) {
      if let text = textField.text, text != "" {
         MethodHelper.showHudWithMessage(message: "Sending...", view: self.view)
         Comment.send(forObjectId: forObjectId, text: text, completion: { (commentO) in
            MethodHelper.hideHUD()
            if let comment = commentO {
                self.comments.append(comment)
                self.tableView.beginUpdates()
                let indexPath = IndexPath(row: 0, section: self.comments.count - 1)
                self.tableView.insertSections([self.comments.count - 1], with: UITableViewRowAnimation.bottom)
                self.tableView.endUpdates()
                if self.tableView.contentOffset.y < self.tableView.contentSize.height {
                    self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.bottom, animated: true)
                }
               self.textField.text = ""
                self.noCommentsImageView.isHidden = true
            }else {
               MethodHelper.showAlert(title: "Error", message: "There was an error posting your comment. Please try again.")
            }
         })
      }
   }
   class func display(viewController:UIViewController, navigationController:UINavigationController?, forObjectId:String) {
        guard let nextView = viewController.storyboard?.instantiateViewController(withIdentifier: ViewControllerIDs.CommentViewController) as? CommentViewController else {return}
        nextView.forObjectId = forObjectId
        if let navController = navigationController {
            navController.pushViewController(nextView, animated: true)
        }else {
            viewController.present(nextView, animated: true, completion: nil)
        }
        
    }

}

extension CommentViewController:UITableViewDelegate,UITableViewDataSource {
   func numberOfSections(in tableView: UITableView) -> Int {
      return comments.count
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return 1
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIDs.CommentTableViewCell, for: indexPath) as? CommentTableViewCell else {return UITableViewCell()}
      cell.populate(forComment: comments[indexPath.section])
      return cell
   }
   
   func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
      return 50
   }
   
   func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      guard  let headerView = Bundle.main.loadNibNamed("TourSitesHeaderView", owner: self, options: nil)?.first as? TourSitesHeaderView else {
         return nil
      }
      headerView.titleLabel.text = ""
      headerView.titleLabel.textColor = UIColor.black
      headerView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
      comments[section].getAuthor { (user) in
         if let author = user {
            headerView.titleLabel.text = author.getNameString() + " - " + self.comments[section].getCreatedAtDateString()
         }
      }
    self.textField.resignFirstResponder()
      
      return headerView
   }
   
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return UITableViewAutomaticDimension
   }
   
   func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
      return UITableViewAutomaticDimension
      
   }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let trash = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
            let alertController = UIAlertController(title: "Delete?", message: "You are about to delete this comment. Are you sure you want to continue?", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
            alertController.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { (action) in
                MethodHelper.showHudWithMessage(message: "Deleting...", view: self.view)
                self.comments[indexPath.section].deleteComment(completion: { (success) in
                    MethodHelper.hideHUD()
                    if success {
                        MethodHelper.showAlert(title: "Success", message: "Your comment was successfully deleted.")
                        self.comments.remove(at: indexPath.section)
                        if self.comments.count == 0 {
                            self.tableView.reloadData()
                            self.noCommentsImageView.isHidden = false
                        }else {
                            self.tableView.beginUpdates()
                            self.tableView.deleteSections([self.comments.count - 1], with: UITableViewRowAnimation.left)
                            self.tableView.endUpdates()
                        }
                    }else {
                        MethodHelper.showAlert(title: "Error", message: "There was an error deleting your comment. Please try again.")
                    }
                })
            }))
            self.present(alertController, animated: true, completion: nil)
        }
        trash.backgroundColor = #colorLiteral(red: 0.9998810887, green: 0.03815252706, blue: 0.003528657835, alpha: 1)
        
        return [trash]
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return comments[indexPath.section].currentUserIsAuthor()
    }
}
