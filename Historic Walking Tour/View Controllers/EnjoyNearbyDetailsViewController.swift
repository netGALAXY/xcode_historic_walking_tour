//
//  EnjoyNearbyDetailsViewController.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 7/24/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit
import MapKit

class EnjoyNearbyDetailsViewController: UIViewController {
    var enjoyNearby:EnjoyNearby!
    @IBOutlet weak var imageTouchView: UIView!
    @IBOutlet weak var distanceIcon: UIButton!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var seeTheRouteButton: UIButton!
    @IBOutlet weak var imageViewYConstraint: NSLayoutConstraint!
    @IBOutlet weak var secondImageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var mainImageView: UIImageView!
    var previousOffset = CGFloat(0)
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var secondImageView: UIImageView!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var websiteButton: UIButton!
    @IBOutlet weak var subtitleHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupMap()
        setupButtons()
        populateEnjoyNearby()
        AnalyticsHelper.logEnjoyPointSelectEvent(enjoyNearby: enjoyNearby)
    }
    
    func setupMap () {
        let annotation = MKPointAnnotation()
        annotation.coordinate = enjoyNearby.location.coordinate
        annotation.title = enjoyNearby.title
        mapView.addAnnotation(annotation)
        mapView.showAnnotations([annotation], animated: true)
    }
    
    func setupButtons () {
        callButton.layer.cornerRadius = 4
        callButton.clipsToBounds = true
        seeTheRouteButton.layer.cornerRadius = 4
        seeTheRouteButton.clipsToBounds = true
        websiteButton.layer.cornerRadius = 4
        websiteButton.clipsToBounds = true
    }
    
    func populateEnjoyNearby () {
        subtitleLabel.text = ""
        if enjoyNearby.url == nil {
            websiteButton.isHidden = true
        }
        if enjoyNearby.phoneNumber == nil {
            callButton.isHidden = true
        }
        self.distanceLabel.text = ""
        enjoyNearby.getDistanceString(forLocation: nil) { (string) in
            if let distance = string {
                self.distanceLabel.text = distance
            }else {
                self.distanceLabel.isHidden = true
                self.distanceIcon.isHidden = true
            }
        }
        titleLabel.text = enjoyNearby.title
        addressLabel.text = enjoyNearby.address
        subtitleLabel.loadHtml(htmlString: enjoyNearby.subtitle) { (success) in
            self.subtitleLabel.sizeToFit()
        }
        if enjoyNearby.subtitle.replacingOccurrences(of: " ", with: "") == "" {
            subtitleHeightConstraint.constant = 0
            view.layoutIfNeeded()
        }
        enjoyNearby.getImage() { (imageO) in
            if let image = imageO {
                self.mainImageView.image = image
                self.imageTouchView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(EnjoyNearbyDetailsViewController.imageViewTapped(tap:))))
            }
        }
        enjoyNearby.getSecondImage { (imageO) in
            if let image = imageO {
                let ratio = image.size.width / image.size.height
                self.secondImageView.image = image
                self.secondImageView.isUserInteractionEnabled = true
                self.secondImageView.isHidden = false
                self.secondImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(EnjoyNearbyDetailsViewController.imageViewTapped(tap:))))
                let newHeight = self.secondImageView.frame.size.width / ratio
                self.secondImageHeightConstraint.constant = newHeight
                self.view.layoutIfNeeded()
            }
        }
        mainImageView.addBlackGradientLayer(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: mainImageView.frame.size.height))
    }
    
    @IBAction func websiteButtonPressed(_ sender: UIButton) {
        TextViewController.display(viewController: self, navigationController: nil, type: .Website, city: nil, url: enjoyNearby.url)
    }
    @IBAction func callButtonPressed(_ sender: UIButton) {
        MethodHelper.call(phoneNumebr:enjoyNearby.phoneNumber)
    }
    @IBAction func seeTheRouteButtonPressed(_ sender: UIButton) {
        enjoyNearby.location.openInMaps(locationTitle: enjoyNearby.title)
    }
    
    
    @objc func imageViewTapped (tap:UITapGestureRecognizer) {
        guard let imageView = tap.view as? UIImageView else {
            self.mainImageView.displayImage(viewController: self)
            return
        }
        imageView.displayImage(viewController: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.size.width, height: mapView.frame.origin.y + mapView.frame.size.height)
        scrollView.isScrollEnabled = true
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        close()
    }
    
    class func display(viewController:UIViewController, navigationController:UINavigationController?, enjoyNearby:EnjoyNearby) {
        guard let nextView = viewController.storyboard?.instantiateViewController(withIdentifier: ViewControllerIDs.EnjoyNearbyDetailsViewController) as? EnjoyNearbyDetailsViewController else {return}
        nextView.enjoyNearby = enjoyNearby
        if let navController = navigationController {
            navController.pushViewController(nextView, animated: true)
        }else {
            viewController.present(nextView, animated: true, completion: nil)
        }
        
    }

}

extension EnjoyNearbyDetailsViewController:UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if mainImageView.frame.origin.y <= 60 {
            self.imageViewYConstraint.constant =  -(scrollView.contentOffset.y * 0.6)
        }
    }
}
