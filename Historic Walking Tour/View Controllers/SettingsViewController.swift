//
//  SettingsViewController.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 7/25/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit
import BEMCheckBox

class SettingsViewController: UIViewController {

    @IBOutlet weak var kmCheckboxContainerView: UIView!
    @IBOutlet weak var milesCheckboxContainerView: UIView!
    @IBOutlet weak var notificationsCheckboxContainerView: UIView!
    @IBOutlet weak var buildInfoLabel: UILabel!
    @IBOutlet weak var buildInfoContainerView: UIView!
    @IBOutlet weak var termsAndConditionsContainerView: UIView!
    @IBOutlet weak var privacyPolicyContainerView: UIView!
    @IBOutlet weak var customerSupportContainerView: UIView!
    @IBOutlet weak var faqContainerView: UIView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var tutorialContainerView: UIView!
    var notificationsCheckbox:BEMCheckBox!
    var milesCheckbox:BEMCheckBox!
    var kmCheckbox:BEMCheckBox!
    @IBOutlet weak var disableTouchIDContainerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        addCheckboxes()
        privacyPolicyContainerView.isHidden = Config.current().privacyPolicy == nil
        termsAndConditionsContainerView.isHidden = Config.current().termsAndConditions == nil
        disableTouchIDContainerView.isHidden = UserDefaults.standard.string(forKey: UserDefaultKeys.savedEmail) == nil

        emailLabel.text = User.getCurrentUser()?.email
        setLocationLabel()
        buildInfoLabel.text = MethodHelper.getVerisionAndBuildString()
        tutorialContainerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SettingsViewController.tutorialContainerViewTapped(_:))))
        faqContainerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SettingsViewController.faqContainerViewTapped(_:))))
        buildInfoContainerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SettingsViewController.versionContainerViewTapped(_:))))
        customerSupportContainerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SettingsViewController.customerSupportContainerViewTapped(_:))))
        termsAndConditionsContainerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SettingsViewController.termsAndConditionsContainerViewTapped(_:))))
        privacyPolicyContainerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SettingsViewController.privacyPolicyContainerViewTapped(_:))))
        
        disableTouchIDContainerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SettingsViewController.disableTouchIDContainerViewTapped(_:))))

    }
    
    func addCheckboxes () {
        notificationsCheckbox = BEMCheckBox(frame: CGRect(x: 0, y: 0, width: notificationsCheckboxContainerView.frame.size.width, height: notificationsCheckboxContainerView.frame.size.height))
        notificationsCheckboxContainerView.addSubview(notificationsCheckbox)
        notificationsCheckbox.bindFrameToSuperviewBounds()
        notificationsCheckbox.boxType = .square
        notificationsCheckbox.delegate = self
        
        milesCheckbox = BEMCheckBox(frame: CGRect(x: 0, y: 0, width: milesCheckboxContainerView.frame.size.width, height: milesCheckboxContainerView.frame.size.height))
        milesCheckboxContainerView.addSubview(milesCheckbox)
        milesCheckbox.bindFrameToSuperviewBounds()
        milesCheckbox.boxType = .square
        milesCheckbox.delegate = self
        
        kmCheckbox = BEMCheckBox(frame: CGRect(x: 0, y: 0, width: kmCheckboxContainerView.frame.size.width, height: kmCheckboxContainerView.frame.size.height))
        kmCheckboxContainerView.addSubview(kmCheckbox)
        kmCheckbox.bindFrameToSuperviewBounds()
        kmCheckbox.boxType = .square
        kmCheckbox.delegate = self
        
        setCheckboxes()
    }
    
    func setCheckboxes () {
        guard let user = User.getCurrentUser() else {return}
        notificationsCheckbox.setOn(user.allowNotifications, animated: false)
        milesCheckbox.setOn(user.displayMiles, animated: false)
        kmCheckbox.setOn(!user.displayMiles, animated: false)
    }
    
    @objc func disableTouchIDContainerViewTapped(_ tap:UITapGestureRecognizer) {
        let alert = UIAlertController(title: "Disable Touch ID", message: "You are about to disable touch ID. Are you sure?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { (action) in
            UserDefaults.standard.removeObject(forKey: UserDefaultKeys.savedEmail)
            UserDefaults.standard.removeObject(forKey: UserDefaultKeys.savedPassword)
            UserDefaults.standard.synchronize()
            self.disableTouchIDContainerView.isHidden = true
        }))
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    @objc func tutorialContainerViewTapped (_ tap:UITapGestureRecognizer) {
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonPressed(sender:UIButton) {
        close()
    }
    
    @objc func termsAndConditionsContainerViewTapped (_ tap:UITapGestureRecognizer) {
        TextViewController.display(viewController: self, navigationController: navigationController, type: .TermsAndConditions, city: nil, url: nil)
    }
    
    @objc func customerSupportContainerViewTapped (_ tap:UITapGestureRecognizer) {
        ContactUsViewController.display(viewController: self)
    }
    
    @objc func privacyPolicyContainerViewTapped (_ tap:UITapGestureRecognizer) {
        TextViewController.display(viewController: self, navigationController: navigationController, type: .PrivacyPolicy, city: nil, url: nil)
    }
    
    @objc func faqContainerViewTapped (_ tap:UITapGestureRecognizer) {
        TextViewController.display(viewController: self, navigationController: navigationController, type: .FAQ, city: nil, url: nil)
    }
    
    @objc func versionContainerViewTapped (_ tap:UITapGestureRecognizer) {
        netGALAXYViewController.display(viewController: self, navigationController: nil) 
    }
    
    func setLocationLabel () {
        MethodHelper.getCurrentLocation { (locationO) in
            if let location = locationO {
                MethodHelper.getCityAndStateFromLocation(location, completion: { (city, state) -> Void in
                    var cityString = ""
                    if let c = city {
                        cityString = c
                    }
                    if let state = state {
                        if cityString != "" {
                            self.locationLabel.text = "\(cityString), \(state)"
                        }else {
                            self.locationLabel.text = state
                        }
                    }else {
                        self.locationLabel.text = cityString
                    }
                })
            }
        }
    }
    

    class func display(viewController:UIViewController, navigationController:UINavigationController?) {
        guard let nextView = viewController.storyboard?.instantiateViewController(withIdentifier: ViewControllerIDs.SettingsViewController) as? SettingsViewController else {return}
        if let navController = navigationController {
            navController.pushViewController(nextView, animated: true)
        }else {
            viewController.present(nextView, animated: true, completion: nil)
        }
        
    }

}

extension SettingsViewController:BEMCheckBoxDelegate {
    func didTap(_ checkBox: BEMCheckBox) {
        guard let user = User.getCurrentUser() else {
            LoginViewController.display(viewController: self, navigationController: nil)
            return
        }
        if checkBox == notificationsCheckbox {
            user.setAllowNotifications(allowNotifications: notificationsCheckbox.on, completion: { (success) in
                self.setCheckboxes()
            })
        }else if checkBox == milesCheckbox {
            kmCheckbox.on = !milesCheckbox.on
            user.setDisplayMiles(displayMiles: milesCheckbox.on, completion: { (success) in
                self.setCheckboxes()
            })
        }else {
            milesCheckbox.on = !kmCheckbox.on
            user.setDisplayMiles(displayMiles: milesCheckbox.on, completion: { (success) in
                self.setCheckboxes()
            })
        }
    }
}
