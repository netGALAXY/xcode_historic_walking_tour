//
//  PointDetailsViewController.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 7/24/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit
import MapKit

class PointDetailsViewController: UIViewController {
    var point:Point!
    
    @IBOutlet weak var frenchPlayerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var frenchMediaPlayerWebView: UIWebView!
    @IBOutlet weak var englishLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var englishLabel: UILabel!
    @IBOutlet weak var rulerAdImageView: UIImageView!
   @IBOutlet weak var mediaPlayerHeightConstraint: NSLayoutConstraint!
   @IBOutlet weak var subtitleHeightConstraint: NSLayoutConstraint!
   @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var imageTouchView: UIButton!
    @IBOutlet weak var distanceIcon: UIButton!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var adImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var adImageView: UIImageView!
    @IBOutlet weak var seeTheRouteButton: UIButton!
    @IBOutlet weak var enjoyNearbyButton: UIButton!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var imageViewYConstraint: NSLayoutConstraint!
   @IBOutlet weak var mapView: MKMapView!
   @IBOutlet weak var scrollView: UIScrollView!
   @IBOutlet weak var detailsLabel: UILabel!
   @IBOutlet weak var mediaPlayerWebView: UIWebView!
   @IBOutlet weak var addressLabel: UILabel!
   @IBOutlet weak var titleLabel: UILabel!
   @IBOutlet weak var mainImageView: UIImageView!
   var previousOffset = CGFloat(0)
    @IBOutlet weak var rulerAdImageViewHeightConstraint: NSLayoutConstraint!
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupMap()
        setupButtons()
        populatePoint()
        loadAd(forBanner: true)
        loadAd(forBanner: false)
        setupMediaPlayer()
        loadDetails()
        AnalyticsHelper.logPointSelectEvent(point: point)
    }
    
    func populatePoint () {
        favoriteButton.alpha = point.isFavorite() ? 1 : 0.5
        self.distanceLabel.text = ""
        self.detailsLabel.text = ""
        self.titleLabel.text = ""
        self.subtitleLabel.text = ""
        point.getDistanceString { (string) in
            if let distance = string {
                self.distanceLabel.text = distance
            }else {
                self.distanceLabel.isHidden = true
                self.distanceIcon.isHidden = true
            }
        }
        titleLabel.text = point.title
        addressLabel.text = point.address
        subtitleLabel.text = point.subtitle
      if point.subtitle == nil {
         subtitleHeightConstraint.constant = 0
         view.layoutIfNeeded()
      }
        point.getImage(forAd: false, forRulerAd: false) { (imageO) in
            if let image = imageO {
                self.mainImageView.image = image
                self.imageTouchView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(PointDetailsViewController.imageTouchViewTapped(_:))))
            }
        }
        mainImageView.addBlackGradientLayer(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: mainImageView.frame.size.height))
    }
    
    func loadAd (forBanner:Bool) {
        var imageView = self.adImageView
        if !forBanner {
            imageView = self.rulerAdImageView
        }
        point.getImage(forAd: forBanner, forRulerAd: !forBanner) { (imageO) in
            guard let imageView = imageView else {return}
            if let image = imageO {
                let ratio = image.size.width / image.size.height
                imageView.image = image
                imageView.isUserInteractionEnabled = true
                imageView.isHidden = false
                let newHeight = imageView.frame.size.width / ratio
                if forBanner {
                    self.adImageViewHeightConstraint.constant = newHeight
                }else {
                    self.rulerAdImageViewHeightConstraint.constant = newHeight
                }
                self.view.layoutIfNeeded()
            }else if forBanner {
                self.adImageView.image = nil
                self.adImageView.isUserInteractionEnabled = false
                self.adImageView.isHidden = true
            }
        }
        self.adImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(PointDetailsViewController.adImageViewTapped(tap:))))
        self.rulerAdImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(PointDetailsViewController.rulerAdImageViewTapped(tap:))))

    }
    
    func setupMediaPlayer () {
        mediaPlayerWebView.scrollView.isScrollEnabled = false
        mediaPlayerWebView.backgroundColor = UIColor.white
        if let audioHTML = getAudioHtml(audioUrl: point.audioUrl) {
            mediaPlayerWebView.loadHTMLString(audioHTML, baseURL: nil)
        }else {
         mediaPlayerHeightConstraint.constant = 0
            englishLabelHeightConstraint.constant = 0
         view.layoutIfNeeded()
      }
        
        if let audioHTML = getAudioHtml(audioUrl: point.frenchAudioUrl) {
            frenchMediaPlayerWebView.loadHTMLString(audioHTML, baseURL: nil)
        }else {
            englishLabelHeightConstraint.constant = 0
            frenchPlayerHeightConstraint.constant = 0
            view.layoutIfNeeded()
        }
    }
    
    func loadDetails () {
        if let html = getDetailsHtml() {
            MethodHelper.showHudWithMessage(message: "Loading...", view: self.view)
            var blankString = ""
            for _ in 0..<300 {
                blankString += "-"
            }
            self.detailsLabel.text = html
            self.detailsLabel.textColor = UIColor.clear
            self.detailsLabel.sizeToFit()
            detailsLabel.loadHtml(htmlString: html, completion: { (success) in
                
                MethodHelper.hideHUD()
            })
        }
    }
    
    func setupMap () {
        let annotation = MKPointAnnotation()
        annotation.coordinate = point.location.coordinate
        annotation.title = point.title
        mapView.addAnnotation(annotation)
        mapView.showAnnotations([annotation], animated: true)
    }
    
    func setupButtons () {
        enjoyNearbyButton.layer.cornerRadius = 4
        enjoyNearbyButton.clipsToBounds = true
        seeTheRouteButton.layer.cornerRadius = 4
        seeTheRouteButton.clipsToBounds = true
    }
    
    @objc func adImageViewTapped(tap:UITapGestureRecognizer) {
        guard let urlString = point.adUrl, let adUrl = URL(string: urlString) else {return}
        UIApplication.shared.openURL(adUrl)
    }
    
    @objc func rulerAdImageViewTapped(tap:UITapGestureRecognizer) {
        guard let urlString = point.rulerAdUrl, let adUrl = URL(string: urlString) else {return}
        UIApplication.shared.openURL(adUrl)
    }
    
    func getDetailsHtml ()->String? {
        guard let details = point.details else {return nil}
        return "<font face=\"AvenirNext-Regular\" size=\"5\">" + details + "</font>"
    }
   
   override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
    if adImageView.image == nil {
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.size.width, height: mapView.frame.origin.y + mapView.frame.size.height)
    }else {
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.size.width, height: mapView.frame.origin.y + mapView.frame.size.height + adImageView.frame.size.height)
    }
      scrollView.isScrollEnabled = true
   }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        close()
    }
    
    class func display(viewController:UIViewController, navigationController:UINavigationController?, point:Point) {
        if point.currentUserHasAccess() {
            if let purchased = point.city?.isPurchased(), !purchased {
                User.incrementFreeListens()
            }
            contineToView(viewController: viewController, navigationController: navigationController, point: point)
        }else {
            displayPremiumDialog(viewController: viewController, navigationController: navigationController, point: point)
        }
    }
    
    private class func displayPremiumDialog (viewController:UIViewController, navigationController:UINavigationController?, point:Point) {
        guard let city = point.city else {
            contineToView(viewController: viewController, navigationController: navigationController, point: point)
            return
        }
        let alertController = UIAlertController(title: "Premium Location", message: "This point is only available to users who have purchased " + city.title + ". Would you like to purchase " + city.title + "?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "No, thanks", style: .default, handler: nil))
        alertController.addAction(UIAlertAction(title: "Yes!", style: .default, handler: { (action) in
                       MethodHelper.showHudWithMessage(message: "Loading...", view: viewController.view)
                        city.buy(completion: { (success) in
                            MethodHelper.hideHUD()
                            if success {
                                print("PURCHASED_PRODUCT")
                                self.contineToView(viewController: viewController, navigationController: navigationController, point: point)
                            }else {
                                MethodHelper.showAlert(title: "Error", message: "There was an error purchasing this city. Please try again.")
                            }
                        })
        }))
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    private class func contineToView(viewController:UIViewController, navigationController:UINavigationController?, point:Point) {
        guard let nextView = viewController.storyboard?.instantiateViewController(withIdentifier: ViewControllerIDs.PointDetailsViewController) as? PointDetailsViewController else {return}
        nextView.point = point
        if let navController = navigationController {
            navController.pushViewController(nextView, animated: true)
        }else {
            viewController.present(nextView, animated: true, completion: nil)
        }
    }
   
    private func getAudioHtml (audioUrl:String?) -> String? {
      guard let audioUrl = audioUrl else {return nil}
      return "<!DOCTYPE html> <html> <head> <style> #audioBar { padding: 0; display: block; margin: 0 auto; max-height: 100%; width: 100%; } </style> </head> <body><audio id=\"audioBar\" controls><source src=\"\(audioUrl)\" type=\"audio/mpeg\" controls=\"1\">Your browser does not support the audio element.</audio> </body> </html>"
      
   }
   
    @IBAction func enjoyNearbyButtonPressed(_ sender: UIButton) {
        guard let city = point.city else {return}
        EnjoyNearbyViewController.display(viewController: self, navigationController: navigationController, city: city, point: point)
    }
    
    @IBAction func seeTheRouteButtonPressed(_ sender: UIButton) {
        point.location.openInMaps(locationTitle: point.title)
    }
    
    
    @objc func imageTouchViewTapped(_ sender: UIView) {
        print("IMAGE_TOUCHED")
        mainImageView.displayImage(viewController: self)
    }
    
    @IBAction func favoriteButtonPressed(_ sender: UIButton) {
        MethodHelper.showHudWithMessage(message: "Loading...", view: view)
        point.toggleFavorite { (success) in
            MethodHelper.hideHUD()
            if success {
                sender.alpha = self.point.isFavorite() ? 1 : 0.5
            }else {
                MethodHelper.showAlert(title: "Error", message: "There was an error completing your request. Please try again.")
            }
        }
    }
    
    @IBAction func shareButtonPressed(_ sender: UIButton) {
        point.share(viewController:self)
    }
    
    @IBAction func commentButtonPressed(_ sender: UIButton) {
        CommentViewController.display(viewController: self, navigationController: navigationController, forObjectId: point.objectId)
    }
    
}

extension PointDetailsViewController:UIScrollViewDelegate {

   func scrollViewDidScroll(_ scrollView: UIScrollView) {
    if mainImageView.frame.origin.y <= 60 {
        self.imageViewYConstraint.constant =  -(scrollView.contentOffset.y * 0.6)
   }
    }
}
