//
//  SplashAdViewController.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 8/24/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit

class SplashAdViewController: UIViewController {
    var splashAd:SplashAd!
    @IBOutlet weak var adImageView:UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        MethodHelper.showHudWithMessage(message: "Loading...", view: adImageView)
        splashAd.getImage { (image) in
            MethodHelper.hideHUD()
            guard let image = image else {
                self.close()
                return
            }
            self.adImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SplashAdViewController.adImageViewTapped(tap:))))

            self.adImageView.image = image
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    class func display(viewController:UIViewController, navigationController:UINavigationController?, splashAd:SplashAd) {
        guard let nextView = viewController.storyboard?.instantiateViewController(withIdentifier: ViewControllerIDs.SplashAdViewController) as? SplashAdViewController else {return}
        nextView.splashAd = splashAd
        if let navController = navigationController {
            navController.pushViewController(nextView, animated: true)
        }else {
            viewController.present(nextView, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func dismissButtonPressed (sender:UIButton) {
        close()
    }
    
    @objc func adImageViewTapped(tap:UITapGestureRecognizer) {
        guard let urlString = splashAd.url, let adUrl = URL(string: urlString) else {return}
        UIApplication.shared.openURL(adUrl)
    }

}
