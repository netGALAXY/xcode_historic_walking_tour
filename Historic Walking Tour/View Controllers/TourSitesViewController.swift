//
//  TourSitesViewController.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 7/24/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit

class TourSitesViewController: UIViewController {
    var city:City!
    var points:[Int:[Point]] = [:]
    @IBOutlet weak var upgradeViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var upgradeView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.rowHeight = UITableViewAutomaticDimension
        Point.getTourPoints(forCity: city) { (points) in
            self.points = points
            self.tableView.reloadData()
        }
        setUpgradeView()

    }
    
    func setUpgradeView () {
        if let c = city {
            if !c.isPurchased() {
                upgradeViewHeightConstraint.constant = upgradeViewHeight
                upgradeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(TourSitesViewController.upgradeViewTapped(_:))))
            }else {
                upgradeViewHeightConstraint.constant = 0
            }
            view.layoutIfNeeded()

        }
    }
    
    @objc func upgradeViewTapped (_ tap:UITapGestureRecognizer) {
        MethodHelper.showHudWithMessage(message: "Loading...", view: view)
        city?.buy(completion: { (success) in
            MethodHelper.hideHUD()
            if success {
                self.setUpgradeView()
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        close()
    }
    
    class func display(viewController:UIViewController, navigationController:UINavigationController?, city:City) {
        guard let nextView = viewController.storyboard?.instantiateViewController(withIdentifier: ViewControllerIDs.TourSitesViewController) as? TourSitesViewController else {return}
        nextView.city = city
        if let navController = navigationController {
            navController.pushViewController(nextView, animated: true)
        }else {
            viewController.present(nextView, animated: true, completion: nil)
        }
        
    }

}

extension TourSitesViewController:UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return points.keys.count + 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 || section == points.keys.count + 1 {
            return 1
        }else if let array = points[section] {
            return array.count
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0, let cell = tableView.dequeueReusableCell(withIdentifier: CellIDs.TourSitesTextTableViewCell, for: indexPath) as? TourSitesTextTableViewCell {
            cell.populate(forCity: city, viewController: self, tableView: tableView, forIndexPath: indexPath)
            return cell
        }else if indexPath.section > 0 && indexPath.section < points.keys.count + 1, let cell = tableView.dequeueReusableCell(withIdentifier: CellIDs.TourSitesPointTableViewCell, for: indexPath) as? TourSitesPointTableViewCell {
            cell.titleLabel.text = ""
            if let array = points[indexPath.section], array.count > indexPath.row {
                cell.populate(forPoint: array[indexPath.row])
            }
            return cell
        }else if indexPath.section == points.keys.count + 1 {
            return  tableView.dequeueReusableCell(withIdentifier: CellIDs.TourSitesBottomTableViewCell, for: indexPath)
        }else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 || section == points.keys.count + 1 {
            return 0
        }else {
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard  let headerView = Bundle.main.loadNibNamed("TourSitesHeaderView", owner: self, options: nil)?.first as? TourSitesHeaderView else {
            return nil
        }
        if section == 1 {
            headerView.titleLabel.text = city.tourSitesDay1Text
        }else if section == 2 {
            headerView.titleLabel.text = city.tourSitesDay2Text
        }else if section == 3 {
            headerView.titleLabel.text = city.tourSitesDay3Text
        }else if section == 4 {
            headerView.titleLabel.text = city.tourSitesDay4Text
        }else if section == 5 {
            headerView.titleLabel.text = city.tourSitesDay5Text
        }
        headerView.titleLabel.minimumScaleFactor = 0.5
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelectRowAt:",indexPath.section)
        if indexPath.section == 0 {
            CityHistoryViewController.display(viewController: self, navigationController: navigationController, city: city)
        }else if let array = points[indexPath.section], array.count > indexPath.row && indexPath.section != points.keys.count + 1 {
            PointDetailsViewController.display(viewController: self, navigationController: navigationController, point: array[indexPath.row])
        }
    }
}
