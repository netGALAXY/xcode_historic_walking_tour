//
//  ViewController.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 6/20/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit
import Parse
import LocalAuthentication

class LoginViewController: UIViewController {
    @IBOutlet weak var facebookLoginContainerView: UIView!
    
    @IBOutlet weak var touchIdImageView: UIImageView!
    @IBOutlet weak var touchIdContainerView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    private var useTouchId = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        facebookLoginContainerView.layer.cornerRadius = 4
        loginButton.layer.cornerRadius = 4
        facebookLoginContainerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(LoginViewController.facebookLoginContainerViewTapped(_:))))
        
        if let _ = navigationController {
            cancelButton.isHidden = true
        }
        

        
        setTouchIdButton()
        processSaveEmailAndPassword()


        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    
    func setTouchIdButton () {
        
        if LAContext().canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) {
            touchIdContainerView.layer.cornerRadius = 4
            touchIdContainerView.layer.borderColor = #colorLiteral(red: 0.9333333333, green: 0.3607843137, blue: 0.4117647059, alpha: 1)
            touchIdContainerView.layer.borderWidth = 1
            touchIdContainerView.clipsToBounds = true
            touchIdContainerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(LoginViewController.touchIdContainerViewTapped(_:))))
        }else {
            touchIdContainerView.isHidden = true
        }
    }
    
    func processSaveEmailAndPassword () {
        guard
            let _ = UserDefaults.standard.string(forKey: UserDefaultKeys.savedEmail),
            let _ = UserDefaults.standard.string(forKey: UserDefaultKeys.savedPassword) else {
                return
        }
        displayTouchID()
    }
    
    @objc func touchIdContainerViewTapped (_ tap: UITapGestureRecognizer) {
        displayTouchID()
    }
    
    func displayTouchID() {
        let email = UserDefaults.standard.string(forKey: UserDefaultKeys.savedEmail)
        let password = UserDefaults.standard.string(forKey: UserDefaultKeys.savedPassword)
        let authenticationContext = LAContext()
        authenticationContext.evaluatePolicy(
            .deviceOwnerAuthenticationWithBiometrics,
            localizedReason: email != nil ? "Verify your fingerprint to login." : "Verify your fingerprint to enable touch ID login.",
            reply: { [unowned self] (success, error) -> Void in
                
                DispatchQueue.main.async {
                    if( success ) {
                        self.useTouchId = true
                        
                        if let email = email, let password = password {
                            self.login(email: email, password: password)
                        }else {
                            self.touchIdImageView.image = #imageLiteral(resourceName: "check")
                            self.touchIdImageView.isUserInteractionEnabled = false
                        }
                        
                        
                        
                    }else if let e = error as? NSError, e.code != -2 {
                        MethodHelper.showAlert(title: "Error", message: "There was an error processing your touch ID. Please try again.")
                    }
                }
                
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let _ = User.getCurrentUser() {
            close()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func loginButtonClicked(_ sender: UIButton) {
        if allFieldsAreFilledOut() {
            self.emailTextField.resignFirstResponder()
            self.passwordTextField.resignFirstResponder()
            
            let email = emailTextField.text!.replacingOccurrences(of: " ", with: "").lowercased()
            let password = passwordTextField.text!.replacingOccurrences(of: " ", with: "")
            login(email: email, password: password)
        }
    }
    
    func login (email:String, password:String) {
        MethodHelper.showHudWithMessage(message: "Logging In...", view: view)

        User.login(email: email, password: password, profilePicture: nil, completion: { (success, errorCodeO) in
            MethodHelper.hideHUD()
            if success {
                if (self.useTouchId) {
                    UserDefaults.standard.set(email, forKey: UserDefaultKeys.savedEmail)
                    UserDefaults.standard.set(password, forKey: UserDefaultKeys.savedPassword)
                    UserDefaults.standard.synchronize()
                }else if let savedEmail = UserDefaults.standard.string(forKey: UserDefaultKeys.savedEmail),
                    savedEmail.replacingOccurrences(of: " ", with: "").lowercased() != email.replacingOccurrences(of: " ", with: "").lowercased(){
                    UserDefaults.standard.removeObject(forKey: UserDefaultKeys.savedEmail)
                    UserDefaults.standard.removeObject(forKey: UserDefaultKeys.savedPassword)
                    UserDefaults.standard.synchronize()
                }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationNames.loggedIn), object: nil)
                if let _ = self.navigationController {
                    AppDelegate.setMainViewAsCenterView()
                }else {
                    self.close()
                }
            }else if let code = errorCodeO {
                if code == 101 {
                    MethodHelper.showAlert(title: "Error", message: "The email/password you entered was not valid. Please re-enter your email and password and try again.")
                }else {
                    MethodHelper.showAlert(title: "Error", message: "There was an error logging in. Please try again")
                }
            }else {
                MethodHelper.showAlert(title: "Error", message: "There was an error logging in. Please try again")
            }
        })
    }
    
    func setEmailsAsUsername () {
        PFCloud.callFunction(inBackground: "setEmailForAllUsers", withParameters: nil) { (response, error) in
            print("RESPONSE:", response, "ERROR:", error)
        }
    }
    
    @objc func facebookLoginContainerViewTapped (_ tap:UITapGestureRecognizer) {
        MethodHelper.showHudWithMessage(message: "Logging in...", view: self.view)
        User.loginWithFacebook { (success, newUser) -> Void in
            MethodHelper.hideHUD()
            if success {
                if newUser {
                    User.getCurrentUser()?.setNew(isNew: false)
                    User.unsetCurrentUser()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationNames.loggedIn), object: nil)
                    if let _ = self.navigationController {
                        AppDelegate.setMainViewAsCenterView()
                    }else {
                        self.close()
                    }
                }else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationNames.loggedIn), object: nil)
                    AppDelegate.setMainViewAsCenterView()
                }
            }else {
                MethodHelper.showAlert(title: "Error", message: "There was an error logging in with Facebook. Please try again" )
            }
        }
    }
    
    @IBAction func registerButtonPressed(_ sender: UIButton) {
        RegisterViewController.display(viewController: self, navigationController: navigationController)
    }
    
    @IBAction func forgotPasswordButtonPressed(_ sender: UIButton) {
        User.forgotPassword(viewControlller: self)
    }
    
    fileprivate func allFieldsAreFilledOut () -> Bool {
        guard let username = emailTextField.text, username.replacingOccurrences(of: " ", with: "") != "" else {
            MethodHelper.showAlert(title: "Email Required", message:"An email is required. Please enter your email and try again" )
            return false
        }
        guard let password = passwordTextField.text, password.replacingOccurrences(of: " ", with: "") != "" else {
            MethodHelper.showAlert(title: "Password Required", message:"A password is required. Please enter your password and try again" )
            return false
        }
        
        return true
    }
    
    class func display (viewController:UIViewController, navigationController:UINavigationController?) {
        guard let nextView = viewController.storyboard?.instantiateViewController(withIdentifier: ViewControllerIDs.LoginViewController) as? LoginViewController else {return}
        if let navController = navigationController {
            navController.pushViewController(nextView, animated: true)
        }else {
            viewController.present(nextView, animated: true, completion: nil)
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        close()
    }
    
}

