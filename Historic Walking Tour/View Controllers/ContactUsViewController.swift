//
//  ContactUsViewController.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 7/25/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import TPKeyboardAvoiding
import Alamofire

class ContactUsViewController: UIViewController {
    @IBOutlet weak var deviceTypeContainerView: UIView!
    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var osLabelContainerView: UIView!
    @IBOutlet weak var osLabel: UILabel!
    @IBOutlet weak var deviceTypeLabel: UILabel!
    @IBOutlet weak var questionTextView: UITextView!
    @IBOutlet weak var questionLabelContainerView: UIView!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var telephoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    
    let placeHolder = "Question"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        nameTextField.layer.borderColor = UIColor.lightGray.cgColor
        nameTextField.layer.borderWidth = 1.0
        emailTextField.layer.borderColor = UIColor.lightGray.cgColor
        emailTextField.layer.borderWidth = 1.0
        telephoneTextField.layer.borderColor = UIColor.lightGray.cgColor
        telephoneTextField.layer.borderWidth = 1.0
        questionLabelContainerView.layer.borderColor = UIColor.lightGray.cgColor
        questionLabelContainerView.layer.borderWidth = 1.0
        questionTextView.layer.borderColor = UIColor.lightGray.cgColor
        questionTextView.layer.borderWidth = 1.0
        deviceTypeContainerView.layer.borderColor = UIColor.lightGray.cgColor
        deviceTypeContainerView.layer.borderWidth = 1.0
        osLabelContainerView.layer.borderColor = UIColor.lightGray.cgColor
        osLabelContainerView.layer.borderWidth = 1.0
        
        nameTextField.addLeftPadding()
        emailTextField.addLeftPadding()
        telephoneTextField.addLeftPadding()

        sendButton.layer.cornerRadius = 4.0
        sendButton.clipsToBounds = true
        
        questionTextView.text = placeHolder
        questionTextView.textColor = UIColor.lightGray
        
        questionLabelContainerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ContactUsViewController.questionLabelContainerViewTapped(_:))))
        deviceTypeContainerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ContactUsViewController.deviceTypeContainerViewTapped(_:))))
        osLabelContainerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ContactUsViewController.osLabelContainerViewTapped(_:))))
        
        if Config.current().contactUsQuestionType.count > 0 {
            self.questionLabel.text = Config.current().contactUsQuestionType[0]
        }
        if Config.current().contactUsDeviceType.count > 0 {
            self.deviceTypeLabel.text = Config.current().contactUsDeviceType[0]
        }
        if Config.current().contactUsOSType.count > 0 {
            self.osLabel.text = Config.current().contactUsOSType[0]
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.size.width, height: sendButton.frame.origin.y  + sendButton.frame.size.height + 100)
        scrollView.isScrollEnabled = true
    }
    
    @objc func questionLabelContainerViewTapped (_ tap:UITapGestureRecognizer) {
        self.view.endEditing(true)
        var rows = Config.current().contactUsQuestionType
        ActionSheetStringPicker.show(withTitle: nil, rows: rows, initialSelection: 0, doneBlock: { (picker, selectedIndex, value) in
            self.questionLabel.text = rows[selectedIndex]
        }, cancel: nil, origin: questionLabel)
    }
    
    @objc func deviceTypeContainerViewTapped (_ tap:UITapGestureRecognizer) {
        self.view.endEditing(true)
        var rows = Config.current().contactUsDeviceType
        ActionSheetStringPicker.show(withTitle: nil, rows: rows, initialSelection: 0, doneBlock: { (picker, selectedIndex, value) in
            self.deviceTypeLabel.text = rows[selectedIndex]
        }, cancel: nil, origin: deviceTypeLabel)
    }
    
    @objc func osLabelContainerViewTapped (_ tap:UITapGestureRecognizer) {
        self.view.endEditing(true)
        var rows = Config.current().contactUsOSType
        ActionSheetStringPicker.show(withTitle: nil, rows: rows, initialSelection: 0, doneBlock: { (picker, selectedIndex, value) in
            self.osLabel.text = rows[selectedIndex]
        }, cancel: nil, origin: osLabel)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonPressed (sender:UIButton) {
        close()
    }
    
    @IBAction func sendButtonPressed(_ sender: UIButton) {
        if allFieldsAreFilledOut() {
            guard
                let name = nameTextField.text,
                let email = emailTextField.text,
                let telephone = telephoneTextField.text,
                let questionType = questionLabel.text,
                let questionText = questionTextView.text,
                let deviceType = deviceTypeLabel.text,
                let osType = osLabel.text,
                let url = URL(string: Config.current().contactFormUrl) else {return}
            
            let params:[String:String] = ["name": name, "email":email, "telephone": telephone, "question_type":questionType, "question_text":questionText, "device_type":deviceType, "os_type":osType]
            MethodHelper.showHudWithMessage(message: "Sending...", view: self.view)
            Alamofire.request(url, method: .post, parameters: params).responseJSON { (response) in
                MethodHelper.hideHUD()
                if response.result.isSuccess {
                    self.dismiss(animated: true, completion: {
                        MethodHelper.showAlert(title: "Thank You!", message: "Thank you for your message. We will be in touch shortly.")
                    })
                }else {
                    MethodHelper.showAlert(title: "Error", message: "There was an error sending your message. Please try again.")
                }
            }
        }
    }
    
    func allFieldsAreFilledOut () -> Bool {
        guard let name = nameTextField.text?.replacingOccurrences(of: " " , with: ""), name != "" else {
            MethodHelper.showAlert(title: "Name Required", message: "Your name is required. Please enter your name and try again.")
            return false
        }
        
        guard let email = emailTextField.text?.replacingOccurrences(of: " ", with: ""), email != "" else {
            MethodHelper.showAlert(title: "Email Required", message: "Your email address is required. Please enter your email address and try again.")
            return false
        }
        
        guard let text = questionTextView.text, text.replacingOccurrences(of: " ", with: "") != "", text.replacingOccurrences(of: " ", with: "") != placeHolder else {
            MethodHelper.showAlert(title: "Question Required", message: "A question is required. Please enter your question and try again.")
            return false
        }
        
        return true
        
        
    }
    
    class func display(viewController:UIViewController) {
        guard let nextView = viewController.storyboard?.instantiateViewController(withIdentifier: ViewControllerIDs.ContactUsViewController) as? ContactUsViewController else {return}
      viewController.present(nextView, animated: true, completion: nil)
    }
   
}

extension ContactUsViewController:UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = placeHolder
            textView.textColor = UIColor.lightGray
        }
    }
}

