//
//  CityMainViewController.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 6/30/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit

class CityMainViewController: UIViewController {
    var city:City!
    
    @IBOutlet weak var upgradeButton: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    var numberOfRows = 5
    var timer:Timer?
    var currentImageIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        city.loadSliderImages()
        
        city.getImage { (image) in
            self.backgroundImageView.image = image
        }
        AnalyticsHelper.logCitySelectEvent(city: city)
        
//        tableView.scrollToRow(at: IndexPath(row: (numberOfRows/2), section: 0), at: UITableViewScrollPosition.middle, animated: true)
        
        setUpradeButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true, block: { (timer) in
            guard self.city.sliderImages.count > 0 else {return}
            self.currentImageIndex += 1
            if self.currentImageIndex == self.city.sliderImages.count {
                self.currentImageIndex = 0
            }
            self.translate(image: self.city.sliderImages[self.currentImageIndex])
        })
    }
    
    func setUpradeButton () {
        if city.isPurchased() {
            upgradeButton.isHidden = true
        }else {
            upgradeButton.isHidden = false
            upgradeButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CityMainViewController.upgradeViewTapped(_:))))
        }
    }
    
    @objc func upgradeViewTapped (_ tap:UITapGestureRecognizer) {
        MethodHelper.showHudWithMessage(message: "Loading...", view: view)
        city?.buy(completion: { (success) in
            MethodHelper.hideHUD()
            if success {
                self.setUpradeButton()
            }
        })
    }
    
    func translate (image:UIImage) {
        let slideTransition = CATransition()
        // CATransition * slideTransition; instance variable
        slideTransition.duration = 0.4
        slideTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        slideTransition.type = kCATransitionPush
        slideTransition.subtype = kCATransitionFromRight
        
        backgroundImageView.layer.add(slideTransition, forKey: nil)
        backgroundImageView.image = image
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
        timer = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    class func display (forCity:City, viewController:UIViewController, navigationController:UINavigationController?) {
        guard let nextView = viewController.storyboard?.instantiateViewController(withIdentifier: ViewControllerIDs.CityMainViewController) as? CityMainViewController else {return}
        nextView.city = forCity
        if let navController = navigationController {
            navController.pushViewController(nextView, animated: true)
        }else {
            viewController.present(nextView, animated: true, completion: nil)
        }
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        close()
    }
    
}

extension CityMainViewController:UITableViewDelegate,UITableViewDataSource {
    class MenuTitles {
        static let howToUse = "How to Use"
        static let tour = "Tour"
        static let enjoyNearby = "Enjoy Nearby"
        static let tourSites = "Tour Sites"
        static let more = "More"
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let rowHeight = tableView.frame.size.height / CGFloat(numberOfRows)
        if rowHeight < 110 {
            return 110
        }else {
            return rowHeight
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIDs.CityMenuTableViewCell, for: indexPath) as? CityMenuTableViewCell else {return UITableViewCell()}
        
        if indexPath.row == 0 {
            cell.titleLabel.text = MenuTitles.tour
            cell.menuImageView.image = #imageLiteral(resourceName: "tour")
        }else if indexPath.row == 1 {
            cell.titleLabel.text = MenuTitles.enjoyNearby
            cell.menuImageView.image = #imageLiteral(resourceName: "enjoy_nearby")
        }else if indexPath.row == 2 {
            cell.titleLabel.text = MenuTitles.tourSites
            cell.menuImageView.image = #imageLiteral(resourceName: "tour_site")
        }else if indexPath.row == 3 {
            cell.titleLabel.text = MenuTitles.howToUse
            cell.menuImageView.image = #imageLiteral(resourceName: "stop")
        }else if indexPath.row == 4 {
            cell.titleLabel.text = MenuTitles.more
            cell.menuImageView.image = #imageLiteral(resourceName: "more")
        }
        
        return cell
    }
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      guard let cell = tableView.cellForRow(at: indexPath) as? CityMenuTableViewCell else {return}
      if cell.titleLabel.text == MenuTitles.tour {
        TourViewController.display(viewController: self, navigationController: navigationController, city: city, forType: .tourSites, displayMapFirst: true)
      }else if cell.titleLabel.text == MenuTitles.howToUse {
        TextViewController.display(viewController: self, navigationController: navigationController, type: .HowToUse, city: city, url: nil)
      }else if cell.titleLabel.text == MenuTitles.enjoyNearby {
        EnjoyNearbyViewController.display(viewController: self, navigationController: navigationController, city: city, point: nil)
      }else if cell.titleLabel.text == MenuTitles.tourSites {
        TourSitesViewController.display(viewController: self, navigationController: navigationController, city: city)
      }else if cell.titleLabel.text == MenuTitles.more {
        MoreViewController.display(viewController: self, navigationController: navigationController, city: city)
    }
   }
}

