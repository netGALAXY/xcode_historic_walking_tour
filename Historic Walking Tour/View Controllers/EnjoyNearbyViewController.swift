//
//  EnjoyNearbyViewController.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 7/24/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit
import MapKit

class EnjoyNearbyViewController: UIViewController {
    var city:City!
    var point:Point?
    var allEnjoyNearbys:[EnjoyNearby] = []
    var enjoyNearbys:[EnjoyNearby] = []
    var selectedCategoryLabel:CategoryLabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewXConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapView: MKMapView!
    var enjoyNearbyDictionary:[String:EnjoyNearby] = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
        MethodHelper.showHudWithMessage(message: "Loading...", view: self.view)
        // Do any additional setup after loading the view.
        EnjoyNearby.getEnjoyNearbys(forCity: city, sortByDistance: true, toLocation: point?.location) { (enjoyNearbys) in
            MethodHelper.hideHUD()
            self.allEnjoyNearbys.append(contentsOf: enjoyNearbys)
            self.enjoyNearbys.append(contentsOf: enjoyNearbys)
            self.addCategoryLabels()
            self.loadEnjoyNearbys(animation: nil)
        }
    }
    
    private func addCategoryLabels () {
        var categories:[Category] = []
        var categoryObjectIds:[String] = []
        for enjoyNearby in allEnjoyNearbys {
            if !categoryObjectIds.contains(enjoyNearby.category.objectId) {
                categoryObjectIds.append(enjoyNearby.category.objectId)
                categories.append(enjoyNearby.category)
            }
        }
        categories.sort { (category1, category2) -> Bool in
            return category1.title < category2.title
        }
        categories.insert(Category.all(), at: 0)
        selectedCategoryLabel = CategoryLabel.addCategories(categories: categories, forScrollView: scrollView, delegate: self)
    }
    
    func loadEnjoyNearbys (animation:UITableViewRowAnimation?) {
        enjoyNearbys.removeAll()
        if selectedCategoryLabel.category.isAll() {
            enjoyNearbys.append(contentsOf: allEnjoyNearbys)
        }else {
            for enjoyNearby in allEnjoyNearbys {
                if enjoyNearby.category.title == selectedCategoryLabel.category.title {
                    enjoyNearbys.append(enjoyNearby)
                }
            }
        }
        if let ani = animation {
            tableView.reloadSections([0], with: ani)
        }else {
            self.tableView.reloadData()
        }
        loadMapView()
    }
    
    func loadMapView () {
        mapView.removeAnnotations(mapView.annotations)
        for enjoyNearby in enjoyNearbys {
            let annotation = MKPointAnnotation()
            annotation.title = enjoyNearby.title
            annotation.subtitle = enjoyNearby.address
            annotation.coordinate = enjoyNearby.location.coordinate
            enjoyNearbyDictionary[enjoyNearby.title] = enjoyNearby
            mapView.addAnnotation(annotation)
        }
        mapView.showAnnotations(mapView.annotations, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    class func display(viewController:UIViewController, navigationController:UINavigationController?, city:City, point:Point?) {
        guard let nextView = viewController.storyboard?.instantiateViewController(withIdentifier: ViewControllerIDs.EnjoyNearbyViewController) as? EnjoyNearbyViewController else {return}
        nextView.city = city
        nextView.point = point
        if let navController = navigationController {
            navController.pushViewController(nextView, animated: true)
        }else {
            viewController.present(nextView, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        tableView.isHidden = true
        close()
    }
    
    @IBAction func listMapButtonPressed(_ sender: UIButton) {
        if tableViewXConstraint.constant < 0 {
            //switching to list
            tableViewXConstraint.constant = 0
            sender.setImage(#imageLiteral(resourceName: "map_pin"), for: .normal)
        }else {
            //switching to map
            tableViewXConstraint.constant = -tableView.frame.size.width
            sender.setImage(#imageLiteral(resourceName: "list"), for: .normal)
        }
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }

}

extension EnjoyNearbyViewController:CategoryLabelDelegate {
    func tapped(categoryLabel: CategoryLabel) {
        var animationOptions = UITableViewRowAnimation.right
        if selectedCategoryLabel.position < categoryLabel.position {
            animationOptions = .left
        }
        selectedCategoryLabel.backgroundColor = #colorLiteral(red: 0.2005214393, green: 0.6904510856, blue: 0.9354477525, alpha: 1)
        selectedCategoryLabel = categoryLabel
        selectedCategoryLabel.backgroundColor = #colorLiteral(red: 0.1450145841, green: 0.2887423635, blue: 0.6055315733, alpha: 1)
        loadEnjoyNearbys(animation: animationOptions)
    }
}

extension EnjoyNearbyViewController:UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enjoyNearbys.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIDs.EnjoyNearbyTableViewCell, for: indexPath) as? EnjoyNearbyTableViewCell else {return UITableViewCell()}
        
        cell.populate(forEnjoyNearby: enjoyNearbys[indexPath.row], forPoint: point)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        EnjoyNearbyDetailsViewController.display(viewController: self, navigationController: navigationController, enjoyNearby: enjoyNearbys[indexPath.row])
    }
}

extension EnjoyNearbyViewController:MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        if let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "pin") {
            annotationView.annotation = annotation
            return annotationView
        } else {
            let annotationView = MKPinAnnotationView(annotation:annotation, reuseIdentifier:"pin")
            annotationView.isEnabled = true
            annotationView.canShowCallout = true
            
            annotationView.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            
            if let t = annotation.title, let title = t, let enjoyNearby = enjoyNearbyDictionary[title], enjoyNearby.isAdvertiser {
                annotationView.pinTintColor = Config.current().adColor
            }else {
                annotationView.pinTintColor = Config.current().enjoyNearbyColor
            }
            
            return annotationView
        }
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        guard let t = view.annotation?.title, let title = t, let enjoyNearby = enjoyNearbyDictionary[title] else {return}
        EnjoyNearbyDetailsViewController.display(viewController: self, navigationController: navigationController, enjoyNearby: enjoyNearby)
    }
}

