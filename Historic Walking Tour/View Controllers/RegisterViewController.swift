//
//  RegisterViewController.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 6/24/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    @IBOutlet weak var registerButton: UIButton!
    
   @IBOutlet weak var confirmPasswordTextField: UITextField!
   @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        registerButton.layer.cornerRadius = 4
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func registerButtonPressed(_ sender: UIButton) {
        if allFieldsAreFilledOut() {
            MethodHelper.showHudWithMessage(message: "Registering...", view: self.view)
            let email = emailTextField.text!.replacingOccurrences(of: " ", with: "").lowercased()
            let password = passwordTextField.text!.replacingOccurrences(of: " ", with: "")
            User.registerUser(email: email, password: password, completion: { (success, errorCode) in
                MethodHelper.hideHUD()
                if success {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationNames.loggedIn), object: nil)
                    if let _ = self.navigationController {
                        AppDelegate.setMainViewAsCenterView()
                    }else {
                        self.close()
                    }
                }else if errorCode > 0 {
                    if errorCode == 202 {
                        MethodHelper.showAlert(title: "Error", message: "The email you entered is already in use. Please enter a different email and try again.")
                    }else {
                        MethodHelper.showAlert(title: "Error", message: "There was an error logging in. Please try again")
                    }
                }else {
                    MethodHelper.showAlert(title: "Error", message: "There was an error registering. Please try again")
                }
            })
        }
    }
    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        close()
    }
    
    fileprivate func allFieldsAreFilledOut () -> Bool {
        guard let username = emailTextField.text, username.replacingOccurrences(of: " ", with: "") != "" else {
            MethodHelper.showAlert(title: "Email Required", message:"An email is required. Please enter your email and try again" )
            return false
        }
        guard let password = passwordTextField.text, password.replacingOccurrences(of: " ", with: "") != "" else {
            MethodHelper.showAlert(title: "Password Required", message:"A password is required. Please enter your password and try again" )
            return false
        }
      
      guard let confirm = confirmPasswordTextField.text, confirm.replacingOccurrences(of: " ", with: "") != "" else {
         MethodHelper.showAlert(title: "Confirmation Password Required", message:"A confirmation password is required. Please enter your password and try again." )
         return false
      }
      
      guard confirm == password else {
         MethodHelper.showAlert(title: "Passwords Do Not Match", message:"Your passwords did not match. Please re-enter your password and try again." )
         return false
      }
        
        return true
    }
    
    class func display (viewController:UIViewController, navigationController:UINavigationController?) {
        guard let nextView = viewController.storyboard?.instantiateViewController(withIdentifier: ViewControllerIDs.RegisterViewController) as? RegisterViewController else {return}
        if let navController = navigationController {
            navController.pushViewController(nextView, animated: true)
        }else {
            viewController.present(nextView, animated: true, completion: nil)
        }
    }

}
