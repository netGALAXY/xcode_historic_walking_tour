//
//  User.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 6/21/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit
import Parse
import ParseFacebookUtilsV4

class User: NSObject {
    private static var currentUser:User?
    var objectId = ""
    var username = ""
    var password = ""
    var email = ""
    private var profilePictureFile:PFFile?
    private var profilePicture:UIImage?
    var newUser = true
    var accessLevel = 0
    var firstName:String?
    var lastName:String?
    var purchasedObjectIds:[String] = []
    var favoritePoints:[String] = []
    var allowNotifications = true
    var displayMiles = true
    var permanentlyFree = false
    var admin = false
    var freeListens = -1

    init (user:PFUser) {
        super.init()
        if let objId = user.objectId {
            objectId = objId
        }
        
        if let value = user.username {
            self.username = value
        }
        if let value = user.email {
            self.email = value
        }
        if let value = user.object(forKey: PFColumns.User.accessLevel) as? NSNumber {
            self.accessLevel = value.intValue
        }
        if let value = user.password {
            self.password = value
        }
        if let value = user.object(forKey: PFColumns.User.newUser) as? NSNumber {
            self.newUser = value.boolValue
        }
        if let value = user.object(forKey: PFColumns.User.profilePicture) as? PFFile {
            self.profilePictureFile = value
        }
        firstName = user.object(forKey: PFColumns.User.firstName) as? String
        lastName = user.object(forKey: PFColumns.User.lastName) as? String
        if let value = user.object(forKey: PFColumns.User.purchasedObjectIds) as? [String] {
            self.purchasedObjectIds = value
        }
        if let value = user.object(forKey: PFColumns.User.favoritePoints) as? [String] {
            self.favoritePoints = value
        }
        if let value = user.object(forKey: PFColumns.User.allowNotifications) as? NSNumber {
            self.allowNotifications = value.boolValue
        }
        if let value = user.object(forKey: PFColumns.User.displayMiles) as? NSNumber {
            self.displayMiles = value.boolValue
        }
        if let value = user.object(forKey: PFColumns.User.permanentlyFree) as? NSNumber {
            self.permanentlyFree = value.boolValue
        }
        if let value = user.object(forKey: PFColumns.User.admin) as? NSNumber {
            self.admin = value.boolValue
        }
        
        if let value = user.object(forKey: PFColumns.User.freeListens) as? NSNumber {
            self.freeListens = value.intValue
        }
    }
    
    static func unsetCurrentUser () {
        currentUser = nil
    }
    
    static func getCurrentUser () -> User? {
        if let user = currentUser {
            return user
        }else if let user = PFUser.current() {
            currentUser = User(user: user)
            return currentUser
        }else {
            return nil
        }

    }
    
    func setNew(isNew:Bool) {
        PFUser.current()?.setObject(NSNumber(value: isNew), forKey: PFColumns.User.newUser)
        PFUser.current()?.saveInBackground(block: { (success, errorO) in
            User.unsetCurrentUser()
        })
    }
    
    func setDisplayMiles(displayMiles:Bool, completion:@escaping (_ success:Bool)->Void) {
        PFUser.current()?.setObject(NSNumber(value: displayMiles), forKey: PFColumns.User.displayMiles)
        PFUser.current()?.saveInBackground(block: { (success, errorO) in
            User.unsetCurrentUser()
            completion(success)
        })
    }
    
    func setAllowNotifications(allowNotifications:Bool, completion:@escaping (_ success:Bool)->Void) {
        PFUser.current()?.setObject(NSNumber(value: allowNotifications), forKey: PFColumns.User.allowNotifications)
        PFUser.current()?.saveInBackground(block: { (success, errorO) in
            User.unsetCurrentUser()
            completion(success)
        })
    }
    
    static func userExists () -> Bool {
        
        return PFUser.current() != nil
    }
    
    class func logOut(_ completion:@escaping (_ success:Bool)->Void) {
        PFUser.logOutInBackground { (error) -> Void in
            if error == nil {
                unsetCurrentUser()
            }
            completion(error == nil)
        }
    }
    
    static func refresh (completion:((_ success:Bool)->Void)?) {
        if let comp = completion, let user = PFUser.current() {
            user.fetchInBackground(block: { (object, error) in
                User.unsetCurrentUser()
                comp(error == nil)
            })
        }else if let user = PFUser.current() {
            do {
                try user.fetchIfNeeded()
                User.unsetCurrentUser()
            }catch {}
        }else {
            completion?(false)
        }
    }
    
    class func forgotPassword (viewControlller:UIViewController) {
        let alertController = UIAlertController(title: "Forgot Password", message: "Please enter the email associated with your account.", preferredStyle: .alert)
        
        
        alertController.addTextField { (textField) in
            textField.keyboardType = .emailAddress
            textField.placeholder = "Email"
        }
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alertController.addAction(UIAlertAction(title: "Done", style: .default, handler: { (action) in
            guard let email = alertController.textFields?.first?.text?.lowercased().replacingOccurrences(of: " ", with: "") else {return}
            MethodHelper.showHudWithMessage(message: "Loading...", view: viewControlller.view!)
            PFUser.requestPasswordResetForEmail(inBackground: email, block: { (success, error) in
                MethodHelper.hideHUD()
                if success {
                    MethodHelper.showAlert(title: "Success", message: "Your password reset link was successfully sent. Please check your email.")
                }else {
                    MethodHelper.showAlert(title: "Error", message: "There was an error requesting your password reset. Please try again.")
                }
            })
        }))
        viewControlller.present(alertController, animated: true, completion: nil)
        
    }
    
    func updateVersionBuildInformation () {
        print("updateVersionBuildInformation \(MethodHelper.getVerisionAndBuildString())")
        PFUser.current()?.setObject("\(MethodHelper.getVerisionAndBuildString())", forKey: PFColumns.User.versionBuildInformation)
        PFUser.current()?.saveEventually()
    }
    
    class func login (email:String, password:String, profilePicture:UIImage?, completion:@escaping (_ success:Bool, _ errorCodeO:Int?)->Void) {
        
        PFUser.logInWithUsername(inBackground: email.lowercased(), password: password, block: { (userO, error) -> Void in
            User.unsetCurrentUser()
            if let e = error {
                // an error occured while attempting login
                if let image = profilePicture, let data = UIImagePNGRepresentation(image), let file = PFFile(name: "profile_picture.png", data: data) {
                    PFUser.current()?.setObject(file, forKey: PFColumns.User.profilePicture)
                }
                PFUser.current()?.saveInBackground()
                completion(false, e._code)
            } else {
                completion(true, nil)
            }
        })
    }
    
    static func loginWithFacebook (_ completion:@escaping (_ success:Bool, _ newUser:Bool)->Void) {
        PFFacebookUtils.logInInBackground(withReadPermissions: Facebook.facebookPermissions) { (userO, error) -> Void in
            guard let _ = PFUser.current(), let user = userO else {
                completion(false, false)
                return
            }
            let request = FBSDKGraphRequest(graphPath: "me", parameters: Facebook.facebookRequestParameters)
            let new = user.isNew
            _ = request?.start(completionHandler: { (connection, result, error) -> Void in
                if let dictionary = result as? NSDictionary {
                    if let email = (dictionary.object(forKey: "email") as? String)?.lowercased(){
                        PFUser.current()!.email = email
                    }
                    let firstName = dictionary.object(forKey: "first_name") as? String
                    let lastName = dictionary.object(forKey: "last_name") as? String
                    
                    if let value = firstName {
                        PFUser.current()!.setObject(value, forKey: PFColumns.User.firstName)
                    }
                    if let value = lastName {
                        PFUser.current()!.setObject(value, forKey: PFColumns.User.lastName)
                    }
                    User.getCurrentUser()?.setProfilePictureFromFacebookUserDictionary(dictionary)
                    PFUser.current()!.saveInBackground(block: { (success, error) -> Void in
                        completion(true, true)
                    })
                    
                }else {
                    print("FACEBOOK_LOGIN_ERROR:",error)
                    completion(false, false)
                }
                
            })
        }
    }
    
    func setProfilePictureFromFacebookUserDictionary (_ dictionary:NSDictionary) {
        print("DICTIONARY \(dictionary)")
        guard let picDict = dictionary.object(forKey: "picture") as? NSDictionary else {return}
        guard let dataDict = picDict.object(forKey: "data") as? NSDictionary else {return}
        guard let url = dataDict.object(forKey: "url") as? String else {return}
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async(execute: { () -> Void in
            let dataO = try? Data(contentsOf: URL(string: url)!)
            DispatchQueue.main.async(execute: { () -> Void in
                guard let data = dataO else {return}
                guard let image = UIImage(data: data) else {return}
                User.saveCurrentUserProfilePicture(image)
            })
            
        })
        
        
    }
    
    fileprivate class func saveCurrentUserProfilePicture (_ image:UIImage) {
        guard let data = UIImagePNGRepresentation(image), let user = User.getCurrentUser() else {return}
        guard let file = PFFile(name: user.objectId+"profilepicture.png", data: data) else {return}
        PFUser.current()?.setObject(file, forKey: PFColumns.User.profilePicture)
        PFUser.current()?.saveInBackground(block: { (success, error) in
            User.unsetCurrentUser()
        })
    }
    
    class func registerUser (email:String, password:String, completion:@escaping (_ success:Bool, _ errorCode:Int)->Void) {
        let user = PFUser()
        user.username = email.replacingOccurrences(of: " ", with: "").lowercased()
        user.email = email.replacingOccurrences(of: " ", with: "").lowercased()
        user.password = password.replacingOccurrences(of: " ", with: "")
        user.setObject(NSNumber(value: true), forKey: PFColumns.User.newUser)

        user.signUpInBackground(block: { (success, error) -> Void in
            if success {
                User.unsetCurrentUser()
                completion(true, 0)
            }else if let e = error {
                completion(false, e._code)
            }else {
                completion(false, 0)
            }
        })
    }
   
   class func getUser (_ forObjectId:String, completion:@escaping (_ user:User?)->Void) {
      guard let query = PFUser.query() else {
         completion(nil)
         return
      }
      query.getObjectInBackground(withId: forObjectId) { (objectO, error) -> Void in
         if let userObject = objectO as? PFUser {
            completion(User(user: userObject))
         }else {
            completion(nil)
         }
      }
   }
   
   func getNameString () -> String {
      var name = ""
      if let first = firstName {
         name = first
      }
      
      if let last = lastName {
         if name != "" {
            name = name + " " + last
         }else {
            name = last
         }
      }
      
      if name == "" {
         name = username
      }
      return name
   }
    
    func isUnderFreeViews () -> Bool {
        return freeListens < Config.current().numberOfFreeListens
    }
    
    class func incrementFreeListens () {
        guard let user = PFUser.current() else {return}
        user.incrementKey(PFColumns.User.freeListens)
        user.saveInBackground { (success, error) in
            User.unsetCurrentUser()
        }
    }
}
