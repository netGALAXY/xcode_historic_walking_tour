//
//  SplashAd.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 8/24/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import Foundation
import UIKit
import Parse

class SplashAd {
    var objectId = ""
    private var imageFile:PFFile?
    private var image:UIImage?
    var url:String?

    init(object:PFObject) {
        if let value = object.objectId {
            objectId = value
        }
        if let value = object.object(forKey: PFColumns.City.imageFile) as? PFFile {
            imageFile = value
        }
        url = object.object(forKey: PFColumns.SplashAd.url) as? String
    }
    
    class func getQuery () -> PFQuery<PFObject> {
        let query = PFQuery(className: PFColumns.SplashAd.className)
        query.whereKey(PFColumns.SplashAd.containerApp, equalTo: PFObject(withoutDataWithClassName: PFColumns.ContainerApp.className, objectId: containerAppObjectId))
        query.whereKey(PFColumns.SplashAd.active, notEqualTo: NSNumber(booleanLiteral: false))
        return query
    }
    
    func getImage (_ completion:@escaping (_ imageO:UIImage?)->Void) {
        if let image = image {
            completion(image)
        }else if let file = imageFile {
            file.getDataInBackground(block: { (dataO, error) in
                if let data = dataO, let image = UIImage(data: data) {
                    self.image = image
                    completion(self.image)
                }else {
                    completion(nil)
                }
            })
        }else {
            completion(nil)
        }
    }
    
    class func get (completion:@escaping (_ splashAd:SplashAd?)->Void) {
        getQuery().getFirstObjectInBackground { (object, error) in
            guard let object = object else {
                completion(nil)
                return
            }
            completion(SplashAd(object: object))
        }
    }
    
}
