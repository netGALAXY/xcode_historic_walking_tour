//
//  Category.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 7/24/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit
import Parse

class Category: NSObject {
    var objectId = ""
    var title = ""
    private static var allTitle = "All categories"
    
    override init() {
        super.init()
    }
    
    init(object:PFObject) {
        super.init()
        if let value = object.objectId {
            objectId = value
        }
        if let value = object.object(forKey: PFColumns.Category.title) as? String {
            title = value
        }
    }
    
    class func all() -> Category {
        let all = Category()
        all.objectId = allTitle
        all.title = allTitle
        return all
    }
    
    func isAll()->Bool {
        return title == Category.allTitle
    }
}
