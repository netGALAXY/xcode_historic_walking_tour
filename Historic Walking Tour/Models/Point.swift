//
//  Point.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 7/7/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit
import Parse

class Point: NSObject {
    var objectId = ""
    var title = ""
    var subtitle:String?
    var address:String?
    var location:CLLocation!
    private var imageFile:PFFile?
    private var image:UIImage?
    var productIdentifier:String?
    var city:City?
    private var distance:Double = 9999999.0
    var restroom = false
    var free = false
    var tourSiteDay = 0
    var tourSiteOrder = 0
    var audioUrl:String?
    var frenchAudioUrl:String?
    var details:String?
    private var adFile:PFFile?
    private var ad:UIImage?
    private var rulerAdFile:PFFile?
    private var rulerAd:UIImage?
    var adUrl:String?
    var rulerAdUrl:String?
    var isAdvertiser = false
    
    init (object:PFObject) {
        super.init() 
        if let value = object.objectId {
            objectId = value
        }
        if let value = object.object(forKey: PFColumns.Point.title) as? String {
            title = value
        }
        if let value =  object.object(forKey: PFColumns.Point.subtitle) as? String, value.replacingOccurrences(of: " ", with: "") != "" {
            subtitle = value
        }
        if let value = object.object(forKey: PFColumns.Point.imageFile) as? PFFile {
            imageFile = value
        }
        if let value = object.object(forKey: PFColumns.Point.city) as? PFObject {
            city = City(object: value)
        }
        if let value = object.object(forKey: PFColumns.Point.location) as? PFGeoPoint {
            location = CLLocation(latitude: value.latitude, longitude: value.longitude)
        }
        if let value = object.object(forKey: PFColumns.Point.address) as? String {
            address = value
        }
        if let value = object.object(forKey: PFColumns.Point.restroom) as? NSNumber {
            self.restroom = value.boolValue
        }
        if let value = object.object(forKey: PFColumns.Point.free) as? NSNumber {
            if restroom {
                self.free = true
            }else {
                self.free = value.boolValue
            }
        }
        if let value = object.object(forKey: PFColumns.Point.tourSiteDay) as? NSNumber {
            self.tourSiteDay = value.intValue
        }
        if let value = object.object(forKey: PFColumns.Point.tourSiteOrder) as? NSNumber {
            self.tourSiteOrder = value.intValue
        }
        if let value =  object.object(forKey: PFColumns.Point.audioUrl) as? String, value.replacingOccurrences(of: " ", with: "") != "" {
            audioUrl = value
        }
        if let value = object.object(forKey: PFColumns.Point.details) as? String {
            details = value
        }
        if let value = object.object(forKey: PFColumns.Point.adFile) as? PFFile {
            adFile = value
        }
        if let value = object.object(forKey: PFColumns.Point.rulerAdFile) as? PFFile {
            rulerAdFile = value
        }
        
        adUrl = object.object(forKey: PFColumns.Point.adUrl) as? String
        if let value = object.object(forKey: PFColumns.Point.isAdvertiser) as? NSNumber {
            self.isAdvertiser = value.boolValue
        }
        rulerAdUrl = object.object(forKey: PFColumns.Point.rulerAdUrl) as? String
        if let value =  object.object(forKey: PFColumns.Point.frenchAudioUrl) as? String, value.replacingOccurrences(of: " ", with: "") != "" {
            frenchAudioUrl = value
        }
    }
    
    func getImage (forAd:Bool, forRulerAd:Bool, _ completion:@escaping (_ imageO:UIImage?)->Void) {
        var downloadImage = self.image
        var downloadFile = self.imageFile
        if forAd {
            downloadImage = ad
            downloadFile = adFile
        }else if forRulerAd {
            downloadImage = rulerAd
            downloadFile = rulerAdFile
        }
        if let image = downloadImage {
            completion(image)
        }else if let file = downloadFile {
            file.getDataInBackground(block: { (dataO, error) in
                if let data = dataO, let image = UIImage(data: data) {
                    if forAd {
                        self.ad = image
                    }else if forRulerAd {
                        self.rulerAd = image
                    }else {
                        self.image = image
                    }
                    completion(image)
                }else {
                    completion(nil)
                }
            })
        }else {
            completion(nil)
        }
    }
    
    class func getQuery () -> PFQuery<PFObject> {
        let query = PFQuery(className: PFColumns.Point.className)
        query.order(byDescending: PFColumns.Point.title)
        query.includeKey(PFColumns.Point.city)
        query.whereKey("active", notEqualTo: NSNumber(booleanLiteral: false))
        return query
    }
    
    class func getPoints (forCity:City?, sortByDistance:Bool, completion:@escaping (_ points:[Point])->Void) {
        let query = getQuery()
        query.whereKey(PFColumns.Point.restroom, notEqualTo: NSNumber(booleanLiteral: true))
        if let city = forCity {
            query.whereKey(PFColumns.Point.city, equalTo: PFObject(withoutDataWithClassName: PFColumns.City.className, objectId: city.objectId))
        }
        if !sortByDistance {
            self.fetchPoints(forQuery: query, forLocation: nil, completion: completion)
        }else {
            MethodHelper.getCurrentLocation(completion: { (locationO) in
                self.fetchPoints(forQuery: query, forLocation: locationO, completion: completion)
                
            })
        }
    }
    
    class func getRestrooms (forCity:City?, completion:@escaping (_ points:[Point])->Void) {
        let query = getQuery()
        if let city = forCity {
            query.whereKey(PFColumns.Point.city, equalTo: PFObject(withoutDataWithClassName: PFColumns.City.className, objectId: city.objectId))
        }
        query.whereKey(PFColumns.Point.restroom, equalTo: NSNumber(booleanLiteral: true))
        MethodHelper.getCurrentLocation(completion: { (locationO) in
            self.fetchPoints(forQuery: query, forLocation: locationO, completion: completion)
            
        })
    }
    
    func isPurchased()->Bool {
        guard let user = User.getCurrentUser() else {return false}
        return user.purchasedObjectIds.contains(objectId)
    }
    
    func currentUserHasAccess () -> Bool {
        guard let c = city else {return true}
        return c.isPurchased() || free || isAdvertiser
    }
    
    class func getFavoritePoints (forCity:City?, forUser:User, completion:@escaping (_ points:[Point])->Void) {
        let query = getQuery()
        if let city = forCity {
            query.whereKey(PFColumns.Point.city, equalTo: PFObject(withoutDataWithClassName: PFColumns.City.className, objectId: city.objectId))
        }
        query.whereKey(PFColumns.objectId, containedIn: forUser.favoritePoints)
        MethodHelper.getCurrentLocation(completion: { (locationO) in
            self.fetchPoints(forQuery: query, forLocation: locationO, completion: completion)
            
        })
    }
    
    private class func fetchPoints (forQuery:PFQuery<PFObject>, forLocation:CLLocation?, completion:@escaping (_ points:[Point])->Void ) {
        forQuery.findObjectsInBackground { (objectsO, error) in
            var points:[Point] = []
            if let objects = objectsO {
                for object in objects {
                    let point = Point(object: object)
                    if let location = forLocation {
                        point.distance = location.distance(from: point.location)
                    }
                    points.append(point)
                }
            }
            let city = points.first?.city
            
            var sortedPoints:[Point] = []
            
            if let c = city, !c.isPurchased() {
                var freePoints:[Point] = []
                var premiumPoints:[Point] = []
                for point in points {
                    if point.free {
                        freePoints.append(point)
                    }else {
                        premiumPoints.append(point)
                    }
                }
                sortedPoints.append(contentsOf: self.sortByDistance(points: freePoints, forLocation: forLocation))
                sortedPoints.append(contentsOf: self.sortByDistance(points: premiumPoints, forLocation: forLocation))
            }else {
                sortedPoints = self.sortByDistance(points: points, forLocation: forLocation)
            }

            completion(sortedPoints)
        }
    }
    
    private class func sortByDistance (points:[Point], forLocation:CLLocation?) -> [Point] {
        if forLocation != nil {
            return points.sorted(by: { (point1, point2) -> Bool in
                return point1.distance < point2.distance
            })
            
        }else {return points}
    }
    
    func isFavorite()->Bool {
        guard let user = User.getCurrentUser() else {return false}
        return user.favoritePoints.contains(objectId)
    }
    
    func toggleFavorite (completion:@escaping (_ success:Bool)->Void) {
        if isFavorite() {
            PFUser.current()?.remove(objectId, forKey: PFColumns.User.favoritePoints)
        }else {
            PFUser.current()?.addUniqueObject(objectId, forKey: PFColumns.User.favoritePoints)
        }
        PFUser.current()?.saveInBackground(block: { (success, error) in
            User.unsetCurrentUser()
            completion(success)
        })
    }
    
    func share (viewController:UIViewController) {
        getImage(forAd: false, forRulerAd: false) { (image) in
            var items:[Any] = []
            items.append("Check out " + self.title + " on Historic Walking Tour! Available for download today! - \(Config.current().shareUrl)")
            if let i = image {
                items.append(i)
            }else {
                items.append(#imageLiteral(resourceName: "app_icon_large"))
            }
            let activityController = UIActivityViewController(activityItems: items, applicationActivities: nil)
            viewController.present(activityController, animated: true, completion: nil)
        }
    }
    
    func getDistanceString (completion:@escaping (_ distanceStringO:String?)->Void) {
        MethodHelper.getCurrentLocation { (locationO) in
            if let location2 = locationO {
                let miles = MethodHelper.getMilesBetweenLocations(location1: self.location, location2: location2)
                var distanceString = ""
                if miles < 0.15 {
                    if let user = User.getCurrentUser(), !user.displayMiles {
                        let meters = 1609.34 * miles
                        distanceString = "\(Int(meters)) meters"
                    }else {
                        let feet = 5280 * miles
                        distanceString = "\(Int(feet)) feet"
                    }
                }else {
                    if let user = User.getCurrentUser(), !user.displayMiles {
                        let km = 1.60934 * miles
                        distanceString = "\(Int(km)) km"
                    }else {
                        distanceString =  "\(String(format: "%.2f", miles)) miles"
                    }
                }
                completion(distanceString)
            }else {
                completion(nil)
            }
        }
    }
    
    class func getTourPoints (forCity:City?, completion:@escaping (_ points:[Int:[Point]])->Void) {
        let query = getQuery()
        query.whereKey(PFColumns.Point.restroom, notEqualTo: NSNumber(booleanLiteral: true))
        if let city = forCity {
            query.whereKey(PFColumns.Point.city, equalTo: PFObject(withoutDataWithClassName: PFColumns.City.className, objectId: city.objectId))
        }
        query.whereKeyExists(PFColumns.Point.tourSiteDay)
        query.findObjectsInBackground { (objectsO, error) in
            var points:[Int:[Point]] = [:]
            if let objects = objectsO {
                for object in objects {
                    let point = Point(object: object)
                    if var array = points[point.tourSiteDay] {
                        array.append(point)
                        points[point.tourSiteDay] = array
                    }else {
                        points[point.tourSiteDay] = [point]
                    }
                }
            }
            for day in points.keys {
                if var array = points[day] {
                    array.sort(by: { (point1, point2) -> Bool in
                        return point1.tourSiteOrder < point2.tourSiteOrder
                    })
                    points[day] = array
                }
            }
            completion(points)
        }
        
    }
    
}
