//
//  City.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 6/29/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit
import Parse
import IAPHelper

class City: NSObject {
    var objectId = ""
    var title = ""
    var subtitle = ""
    private var imageFile:PFFile?
    private var image:UIImage?
    var productIdentifier:String?
    var sliderImages:[UIImage] = []
    var howToUse = ""
    var tourSitesText = ""
    var tourSitesDay1Text:String?
    var tourSitesDay2Text:String?
    var tourSitesDay3Text:String?
    var tourSitesDay4Text:String?
    var tourSitesDay5Text:String?
    private var distance:Double = 9999999.0
    var location:CLLocation?
    var historyText:String?
    var comingSoon = false
    var hasRestrooms = true
    var historyAudioUrl:String?

    init (object:PFObject) {
        super.init()
        if let value = object.objectId {
            objectId = value
        }
        if let value = object.object(forKey: PFColumns.City.title) as? String {
            title = value
        }
        if let value = object.object(forKey: PFColumns.City.subtitle) as? String {
            subtitle = value
        }
        if let value = object.object(forKey: PFColumns.City.imageFile) as? PFFile {
            imageFile = value
        }
        if let value = object.object(forKey: PFColumns.City.productIdentifier) as? String {
            productIdentifier = value
        }
        if let value = object.object(forKey: PFColumns.City.howToUse) as? String {
            howToUse = value
        }
        if let value = object.object(forKey: PFColumns.City.tourSitesText) as? String {
            tourSitesText = value
        }
        tourSitesDay1Text = object.object(forKey: PFColumns.City.tourSitesDay1Text) as? String
        tourSitesDay2Text = object.object(forKey: PFColumns.City.tourSitesDay2Text) as? String
        tourSitesDay3Text = object.object(forKey: PFColumns.City.tourSitesDay3Text) as? String
        tourSitesDay4Text = object.object(forKey: PFColumns.City.tourSitesDay4Text) as? String
        tourSitesDay5Text = object.object(forKey: PFColumns.City.tourSitesDay5Text) as? String
        if let value = object.object(forKey: PFColumns.City.location) as? PFGeoPoint {
            self.location = CLLocation(latitude: value.latitude, longitude: value.longitude)
        }
        historyText = object.object(forKey: PFColumns.City.historyText) as? String
        if let value = object.object(forKey: PFColumns.City.comingSoon) as? NSNumber {
            self.comingSoon = value.boolValue
        }
        if let value = object.object(forKey: PFColumns.City.hasRestrooms) as? NSNumber {
            self.hasRestrooms = value.boolValue
        }
        if let value =  object.object(forKey: PFColumns.City.historyAudioUrl) as? String, value.replacingOccurrences(of: " ", with: "") != "" {
            historyAudioUrl = value
        }
    }
    
    func getImage (_ completion:@escaping (_ imageO:UIImage?)->Void) {
        if let image = image {
            completion(image)
        }else if let file = imageFile {
            file.getDataInBackground(block: { (dataO, error) in
                if let data = dataO, let image = UIImage(data: data) {
                    self.image = image
                    completion(self.image)
                }else {
                    completion(nil)
                }
            })
        }else {
            completion(nil)
        }
    }
    
    func loadSliderImages () {
        if sliderImages.count > 0 {
            return
        }
        let query = PFQuery(className: PFColumns.SliderImage.className)
        query.whereKey(PFColumns.SliderImage.forObjectId, equalTo: objectId)
        query.findObjectsInBackground { (objectsO, error) in
            if let objects = objectsO {
                for object in objects {
                    if let file = object.object(forKey: PFColumns.SliderImage.imageFile) as? PFFile {
                        file.getDataInBackground(block: { (dataO, error) in
                            if let data = dataO, let image = UIImage(data: data) {
                                self.sliderImages.append(image)
                            }
                        })
                    }
                }
            }
        }
    }
    
    class func getQuery () -> PFQuery<PFObject> {
        let query = PFQuery(className: PFColumns.City.className)
        query.whereKey(PFColumns.City.containerApp, equalTo: PFObject(withoutDataWithClassName: PFColumns.ContainerApp.className, objectId: containerAppObjectId))
        query.order(byAscending: PFColumns.City.title)
        query.whereKey(PFColumns.City.active, notEqualTo: NSNumber(booleanLiteral: false))
        return query
    }
    
    class func getAllCities (completion:@escaping (_ cities:[City])->Void) {
        MethodHelper.getCurrentLocation { (location) in
            let query = getQuery()
            query.findObjectsInBackground { (objectsO, error) in
                var cities:[City] = []
                if let objects = objectsO {
                    for object in objects {
                        let city = City(object: object)
                        if let loc = location, let cityLocation = city.location {
                            city.distance = loc.distance(from: cityLocation)
                        }
                        cities.append(city)
                    }
                }
                if let _ = location {
                    cities.sort(by: { (city1, city2) -> Bool in
                        return city1.distance < city2.distance
                    })
                }
                completion(cities)
            }
        }
    }
    func isPurchased()->Bool {
        guard let user = User.getCurrentUser() else {return false}
        if user.permanentlyFree {
            return true
        }
        return user.purchasedObjectIds.contains(objectId)
    }
    func buy(completion:@escaping (_ success:Bool)->Void) {
        guard let product = getProduct() else {
            completion(false)
            return
        }
        IAPShare.sharedHelper().iap.buyProduct(product) { (transaction) in
            if let e = transaction?.error {
                print("TRANSACTION_ERROR:",e)
                completion(false)
            }else {
                self.completePurchase(completion: completion)
            }
        }
    }
    
    private func completePurchase(completion:@escaping (_ success:Bool)->Void) {
        PFUser.current()?.addUniqueObject(objectId, forKey: PFColumns.User.purchasedObjectIds)
        PFUser.current()?.saveInBackground(block: { (success, error) in
            if success {
                AnalyticsHelper.logCityPurchasedEvent(city: self)
                User.unsetCurrentUser()
            }else {
                print("COMPLETE_PURCHASE_ERROR:", error)
            }
            completion(success)
        })
    }
    
    private func getProduct() -> SKProduct? {
        for object in IAPShare.sharedHelper().iap.products {
            print("NO_PRODUCT_FOUND:",productIdentifier, object)
            
            if let productId = self.productIdentifier, let product = object as? SKProduct, product.productIdentifier == productId {
                return product
            }
        }
        return nil
    }
    
    func countTourSites (includeBathrooms:Bool, completion:@escaping (_ count:Int)->Void) {
        let query = Point.getQuery()
        query.whereKey(PFColumns.Point.city, equalTo: PFObject(withoutDataWithClassName: PFColumns.City.className, objectId: objectId))
        query.countObjectsInBackground { (count, error) in
            completion(Int(count))
        }
    }
}
