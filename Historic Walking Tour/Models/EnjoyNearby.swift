//
//  EnjoyNearby.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 7/24/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit
import Parse

class EnjoyNearby: NSObject {
    var objectId = ""
    var title = ""
    var subtitle = ""
    var address:String?
    var location:CLLocation!
    private var imageFile:PFFile?
    private var image:UIImage?
    private var secondImageFile:PFFile?
    private var secondImage:UIImage?
    var city:City?
    private var distance:Double = 9999999.0
    var category:Category!
    var url:String?
    var phoneNumber:String?
    var isAdvertiser = false

    init (object:PFObject) {
        super.init()
        if let value = object.objectId {
            objectId = value
        }
        if let value = object.object(forKey: PFColumns.EnjoyNearby.title) as? String {
            title = value
        }
        if let value = object.object(forKey: PFColumns.EnjoyNearby.subtitle) as? String {
            subtitle = value
        }
        if let value = object.object(forKey: PFColumns.EnjoyNearby.imageFile) as? PFFile {
            imageFile = value
        }
        if let value = object.object(forKey: PFColumns.EnjoyNearby.city) as? PFObject {
            city = City(object: value)
        }
        if let value = object.object(forKey: PFColumns.EnjoyNearby.location) as? PFGeoPoint {
            location = CLLocation(latitude: value.latitude, longitude: value.longitude)
        }
        if let value = object.object(forKey: PFColumns.EnjoyNearby.address) as? String {
            address = value
        }
        if let value = object.object(forKey: PFColumns.EnjoyNearby.category) as? PFObject {
            category = Category(object: value)
        }
        if let value = object.object(forKey: PFColumns.EnjoyNearby.secondImageFile) as? PFFile {
            secondImageFile = value
        }
        if var value = object.object(forKey: PFColumns.EnjoyNearby.url) as? String, value.replacingOccurrences(of: " ", with: "") != "" {
            if !value.contains("http") {
                value = "http://" + value
            }
            url = value
        }
        if let value = object.object(forKey: PFColumns.EnjoyNearby.phoneNumber) as? String, value.replacingOccurrences(of: " ", with: "") != "" {
            phoneNumber = value
        }
        if let value = object.object(forKey: PFColumns.EnjoyNearby.isAdvertiser) as? NSNumber {
            self.isAdvertiser = value.boolValue
        }
    }
    
    func getImage (_ completion:@escaping (_ imageO:UIImage?)->Void) {
        if let image = image {
            completion(image)
        }else if let file = imageFile {
            file.getDataInBackground(block: { (dataO, error) in
                if let data = dataO, let image = UIImage(data: data) {
                    self.image = image
                    completion(self.image)
                }else {
                    completion(nil)
                }
            })
        }else {
            completion(nil)
        }
    }
    
    func getSecondImage (_ completion:@escaping (_ imageO:UIImage?)->Void) {
        if let image = secondImage {
            completion(image)
        }else if let file = secondImageFile {
            file.getDataInBackground(block: { (dataO, error) in
                if let data = dataO, let image = UIImage(data: data) {
                    self.secondImage = image
                    completion(self.secondImage)
                }else {
                    completion(nil)
                }
            })
        }else {
            completion(nil)
        }
    }
    
    class func getQuery () -> PFQuery<PFObject> {
        let query = PFQuery(className: PFColumns.EnjoyNearby.className)
        query.order(byDescending: PFColumns.EnjoyNearby.title)
        query.includeKey(PFColumns.EnjoyNearby.city)
        query.includeKey(PFColumns.EnjoyNearby.category)
        return query
    }
    
    class func getEnjoyNearbys (forCity:City?, sortByDistance:Bool, toLocation:CLLocation?, completion:@escaping (_ enjoyNearbys:[EnjoyNearby])->Void) {
        let query = getQuery()
        if let city = forCity {
            query.whereKey(PFColumns.EnjoyNearby.city, equalTo: PFObject(withoutDataWithClassName: PFColumns.City.className, objectId: city.objectId))
        }
        if !sortByDistance {
            self.fetchEnjoyNearbys(forQuery: query, forLocation: nil, completion: completion)
        }else if let location = toLocation {
            self.fetchEnjoyNearbys(forQuery: query, forLocation: location, completion: completion)
        } else {
            MethodHelper.getCurrentLocation(completion: { (locationO) in
                self.fetchEnjoyNearbys(forQuery: query, forLocation: locationO, completion: completion)
                
            })
        }
    }
    
    private class func fetchEnjoyNearbys (forQuery:PFQuery<PFObject>, forLocation:CLLocation?, completion:@escaping (_ enjoyNearbys:[EnjoyNearby])->Void ) {
        forQuery.limit = 1000
        forQuery.findObjectsInBackground { (objectsO, error) in
            var enjoyNearbys:[EnjoyNearby] = []
            if let objects = objectsO {
                for object in objects {
                    let point = EnjoyNearby(object: object)
                    if let location = forLocation {
                        point.distance = location.distance(from: point.location)
                    }
                    enjoyNearbys.append(point)
                }
            }
            if forLocation != nil {
                enjoyNearbys.sort(by: { (point1, point2) -> Bool in
                    return point1.distance < point2.distance
                })
            }
            completion(enjoyNearbys)
        }
    }
    
    func getDistanceString (forLocation:CLLocation?, completion:@escaping (_ distanceStringO:String?)->Void) {
        if let loc = forLocation {
            completion(getDistanceString(forLocation: loc))
        }else {
            MethodHelper.getCurrentLocation { (locationO) in
                if let location2 = locationO {
                    completion(self.getDistanceString(forLocation: location2))
                }else {
                    completion(nil)
                }
            }
        }
    }
    
    private func getDistanceString(forLocation:CLLocation)->String {
        guard let user = User.getCurrentUser() else {return ""}
        let miles = MethodHelper.getMilesBetweenLocations(location1: self.location, location2: forLocation)
        var distanceString = ""
        if miles < 0.15 {
            if user.displayMiles {
                let feet = 5280 * miles
                distanceString = "\(Int(feet)) feet"
            }else {
                let meters = 1609.34 * miles
                distanceString = "\(Int(meters)) meters"
            }
        }else {
            if user.displayMiles {
                distanceString =  "\(String(format: "%.2f", miles)) miles"
            }else {
                let km = 1.60934 * miles
                distanceString = "\(Int(km)) km"
            }
        }
        return distanceString
    }
}
