//
//  Comment.swift
//  Historic Walking Tour
//
//  Created by Charlie  on 7/27/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit
import Parse

class Comment: NSObject {
   var objectId = ""
   var createdAt = Date()
   private var authorObjectId:String?
   private var author:User?
   var commentText = ""
   var forObjectId = ""
   
   init(object:PFObject) {
      super.init()
      if let value = object.objectId {
         objectId = value
      }
      if let value = object.createdAt {
         createdAt = value
      }
      authorObjectId = object.object(forKey: PFColumns.Comment.authorObjectId) as? String
      if let value = object.object(forKey: PFColumns.Comment.commentText) as? String {
         commentText = value
      }
      if let value = object.object(forKey: PFColumns.Comment.forObjectId) as? String {
         forObjectId = value
      }
   }
   
   class func getQuery () -> PFQuery<PFObject> {
      let query = PFQuery(className: PFColumns.Comment.className)
      query.order(byAscending: PFColumns.createdAt)
      return query
   }
   
   class func getComments (forObjectId:String, completion:@escaping (_ comments:[Comment])->Void) {
      let query = getQuery()
      query.whereKey(PFColumns.Comment.forObjectId, equalTo: forObjectId)
      query.findObjectsInBackground { (objectsO, error) in
         var comments:[Comment] = []
         if let objects = objectsO {
            for object in objects {
               comments.append(Comment(object: object))
            }
         }
         completion(comments)
      }
   }
   
   func currentUserIsAuthor ()->Bool {
      guard let user = User.getCurrentUser(), let authObjectId = authorObjectId, authObjectId.replacingOccurrences(of: " ", with: "") != "" else {return false}
      return authObjectId == user.objectId
   }
   
   class func send (forObjectId:String, text:String, completion:@escaping (_ comment:Comment?)->Void) {
    guard let user = User.getCurrentUser() else {
        completion(nil)
        return
    }
      let object = PFObject(className: PFColumns.Comment.className)
      object.setObject(forObjectId, forKey: PFColumns.Comment.forObjectId)
      object.setObject(text, forKey: PFColumns.Comment.commentText)
      object.setObject(user.objectId, forKey: PFColumns.Comment.authorObjectId)
      object.saveInBackground { (success, error) in
         if success {
            object.fetchIfNeededInBackground(block: { (newObject, error) in
               if let obj = newObject {
                  completion(Comment(object: obj))
               }else {
                  completion(nil)
               }
            })
         }else {
            completion(nil)
         }
      }
   }
   
   func getAuthor (completion:@escaping (_ user:User?)->Void) {
      if let user = author {
         completion(user)
      }else if let objId = authorObjectId {
         User.getUser(objId, completion: { (user) in
            self.author = user
            completion(user)
         })
      }else {
         completion(nil)
      }
   }
   
   func getCreatedAtDateString () -> String {
      let df = DateFormatter()
      df.dateFormat = "MM/dd"
      if Calendar.current.isDateInToday(createdAt) {
         df.dateFormat = "hh:mm a"
      }
      return df.string(from: createdAt)
   }
    
    func deleteComment (completion:@escaping (_ success:Bool)->Void) {
        let object = PFObject(withoutDataWithClassName: PFColumns.Comment.className, objectId: objectId)
        object.deleteInBackground { (success, error) in
            completion(success)
        }
    }
}
