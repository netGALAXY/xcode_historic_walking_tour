//
//  Config.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 6/30/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit
import Parse

class Config: NSObject {
    private static var currentConfig:Config!
    var privacyPolicy:String?
    var termsAndConditions:String?
    var shareUrl = "http://www.fortsumtertours.com/"
    var faq = ""
    var supportEmail = "cvonlehe@netgalaxystudios.com"
    var websiteUrl = "http://www.fortsumtertours.com/"
    var inProduction = true
    var aboutUs = ""
    var franchiseOpportunities = ""
    var contactFormUrl = "http://www.charlesvonlehe.com/contact_us.php"
    var contactUsQuestionType:[String] = []
    var contactUsDeviceType:[String] = []
    var contactUsOSType:[String] = []
    var adColor = UIColor.green
    var premiumColor = UIColor.red
    var freeColor = UIColor.blue
    var enjoyNearbyColor = UIColor.green
    var numberOfFreeListens = 1

    init(pfConfig:PFConfig) {
        super.init()
        if let privacy = pfConfig.object(forKey: PFConfigValues.privacyPolicy) as? String, privacy.replacingOccurrences(of: " ", with: "") != "" {
            self.privacyPolicy = privacy
        }
        if let terms = pfConfig.object(forKey: PFConfigValues.termsAndConditions) as? String, terms.replacingOccurrences(of: " ", with: "") != "" {
            self.termsAndConditions = terms
        }
        
        if let value = pfConfig.object(forKey: PFConfigValues.shareUrl) as? String {
            self.shareUrl = value
        }
        if let value = pfConfig.object(forKey: PFConfigValues.faq) as? String {
            self.faq = value
        }
        if let value = pfConfig.object(forKey: PFConfigValues.supportEmail) as? String {
            self.supportEmail = value
        }
        if let value = pfConfig.object(forKey: PFConfigValues.websiteUrl) as? String {
            self.websiteUrl = value
        }
        if let value = pfConfig.object(forKey: PFConfigValues.inProduction) as? NSNumber {
            self.inProduction = value.boolValue
        }
        if let value = pfConfig.object(forKey: PFConfigValues.aboutUs) as? String {
            self.aboutUs = value
        }
        if let value = pfConfig.object(forKey: PFConfigValues.franchiseOpportunities) as? String {
            self.franchiseOpportunities = value
        }
        if let value = pfConfig.object(forKey: PFConfigValues.contactFormUrl) as? String {
            self.contactFormUrl = value
        }
        if let value = pfConfig.object(forKey: PFConfigValues.contactUsQuestionType) as? [String] {
            self.contactUsQuestionType = value
        }
        if let value = pfConfig.object(forKey: PFConfigValues.contactUsDeviceType) as? [String] {
            self.contactUsDeviceType = value
        }
        if let value = pfConfig.object(forKey: PFConfigValues.contactUsOSType) as? [String] {
            self.contactUsOSType = value
        }
        if let value = pfConfig.object(forKey: PFConfigValues.adColor) as? String {
            self.adColor = UIColor(hex: value.replacingOccurrences(of: " ", with: ""))
        }
        if let value = pfConfig.object(forKey: PFConfigValues.premiumColor) as? String {
            self.premiumColor = UIColor(hex: value.replacingOccurrences(of: " ", with: ""))
        }
        if let value = pfConfig.object(forKey: PFConfigValues.freeColor) as? String {
            self.freeColor = UIColor(hex: value.replacingOccurrences(of: " ", with: ""))
        }
        if let value = pfConfig.object(forKey: PFConfigValues.enjoyNearbyColor) as? String {
            self.enjoyNearbyColor = UIColor(hex: value.replacingOccurrences(of: " ", with: ""))
        }
        if let value = pfConfig.object(forKey: PFConfigValues.numberOfFreeListens) as? NSNumber {
            self.numberOfFreeListens = value.intValue
        }
    }
    
    class func current () -> Config {
        if currentConfig == nil {
            currentConfig = Config(pfConfig: PFConfig.current())
            do {
                let current = try PFConfig.getConfig()
                currentConfig = Config(pfConfig: current)
            }catch {
                
            }
        }
        return currentConfig
    }
}
