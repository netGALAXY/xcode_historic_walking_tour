//
//  EnjoyNearbyTableViewCell.swift
//  Historic Walking Tour
//
//  Created by Charles von Lehe on 7/24/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit

class EnjoyNearbyTableViewCell: UITableViewCell {
    @IBOutlet weak var enjoyNearbyImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func populate(forEnjoyNearby:EnjoyNearby, forPoint:Point?) {
        forEnjoyNearby.getImage { (image) in
            if let im = image {
                self.enjoyNearbyImageView.image = im
            }
        }
        titleLabel.text = forEnjoyNearby.title
        addressLabel.text = forEnjoyNearby.address
        forEnjoyNearby.getDistanceString(forLocation: forPoint?.location) { (distanceString) in
            self.distanceLabel.text = distanceString
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        enjoyNearbyImageView.image = #imageLiteral(resourceName: "placeholder")
        distanceLabel.text = ""
    }
    

}
