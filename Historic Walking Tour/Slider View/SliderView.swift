//
//  LotterSliderView.swift
//  Did I Win
//
//  Created by Charlie  on 4/27/17.
//  Copyright © 2017 netGALAXY Studios. All rights reserved.
//

import UIKit

class SliderView: UIView {
   var delegateO:SliderContainerViewDelegate?
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    var city:City!
    @IBOutlet weak var purchaseButton: UIButton!
    @IBOutlet weak var tourSiteNumberLabel: UILabel!
    
    func populate (city:City, delegate:SliderContainerViewDelegate?) {
        delegateO = delegate
        self.city = city
        city.getImage { (image) in
            self.imageView.image = image
        }
        titleLabel.text = city.title
        subtitleLabel.text = city.subtitle
        if city.comingSoon {
            alpha = 0.5
            purchaseButton.setTitle("Coming Soon".uppercased(), for: .normal)
        }
        
        tourSiteNumberLabel.text = ""
        city.countTourSites(includeBathrooms: false) { (count) in
            if count > 0 {
                self.tourSiteNumberLabel.text  = "\(count) Tour Sites"
            }
        }
   }
    
    override func willMove(toWindow newWindow: UIWindow?) {
        if newWindow == nil {
            NotificationCenter.default.removeObserver(self)
        }else {
            NotificationCenter.default.addObserver(self, selector: #selector(SliderView.purchasesRestored), name: NSNotification.Name(rawValue: NotificationNames.purchasesRestored), object: nil)
        }
    }
    
    @objc private func purchasesRestored () {
        if city.isPurchased() {
            purchaseButton.setTitle("Explore".uppercased(), for: .normal)
        }else {
            purchaseButton.setTitle("Purchase".uppercased(), for: .normal)
        }
    }
    
    

    @IBAction func purchaseButtonPressed(_ sender: UIButton) {
        if let user = User.getCurrentUser(), user.admin || !city.comingSoon {
            delegateO?.clickedExplore(forCity:city)
        }else if User.getCurrentUser() == nil {
            delegateO?.clickedExplore(forCity:city)
        }
    }
    
}
