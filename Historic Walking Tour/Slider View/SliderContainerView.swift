//
//  LotterySliderContainerView.swift
//  Did I Win
//
//  Created by Charlie  on 4/27/17.
//  Copyright © 2017 netGALAXY Studios. All rights reserved.
//

import UIKit

protocol SliderContainerViewDelegate {
    func clickedExplore(forCity:City)
}
class SliderContainerView: UIView {
   var delegateO:SliderContainerViewDelegate?
   
   var widthLength:CGFloat!
   var heightLength:CGFloat!

   var widthConstraint:NSLayoutConstraint!
   var heightConstraint:NSLayoutConstraint!
   var yConstraint:NSLayoutConstraint!
   var originalXConstraint:NSLayoutConstraint!
   var sliderView:SliderView?
   
    init(city:City, viewController:MainViewController, delegate: SliderContainerViewDelegate) {
      delegateO = delegate


      widthLength = UIScreen.main.bounds.size.width - 100
      heightLength = widthLength + 20
      if viewController.sectionImageViewX == 0 {
         viewController.sectionImageViewX = (UIScreen.main.bounds.size.width / 2) - (widthLength / 2)
      }
      let sectionImageViewY = (UIScreen.main.bounds.size.height / 2) - (heightLength / 2) - 30
      super.init(frame: CGRect(x: viewController.sectionImageViewX, y: sectionImageViewY, width: widthLength, height: heightLength))
      self.translatesAutoresizingMaskIntoConstraints = false

      viewController.gestureView.addSubview(self)
      
      widthConstraint = NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: .none, attribute: .notAnAttribute, multiplier: 1, constant: widthLength)
      heightConstraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: .none, attribute: .notAnAttribute, multiplier: 1, constant: heightLength)
      var xConstraint = NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: viewController.gestureView, attribute: .centerX, multiplier: 1, constant: 0)
      if viewController.previousView != nil {
         xConstraint = NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: viewController.previousView, attribute: .trailing, multiplier: 1, constant: 24)
      }else {
         viewController.leadingXConstraint = xConstraint
      }
      originalXConstraint = xConstraint
      yConstraint = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: viewController.gestureView, attribute: .centerY, multiplier: 1, constant: -30)
      viewController.gestureView.addConstraints([widthConstraint, heightConstraint, xConstraint, yConstraint])
      viewController.previousView = self
        addSldierView(city:city)
      
      
   }
   
    func addSldierView (city:City) {
      self.sliderView = Bundle.main.loadNibNamed("SliderView", owner: self, options: nil)?.first as? SliderView
      sliderView?.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)
      
      addSubview(sliderView!)
      
      sliderView?.bindFrameToSuperviewBounds()
        sliderView?.populate(city:city, delegate: delegateO)
   }
   
   required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
    


}
