
//
//  CommentTableViewCell.swift
//  Historic Walking Tour
//
//  Created by Charlie  on 7/27/17.
//  Copyright © 2017 netgalaxystudios. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {
   @IBOutlet var commentLabel:UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   
   func populate (forComment:Comment) {
      commentLabel.text = forComment.commentText
      commentLabel.sizeToFit()
   }
}
